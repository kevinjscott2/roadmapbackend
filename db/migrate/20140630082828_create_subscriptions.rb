class CreateSubscriptions < ActiveRecord::Migration
  def up
    create_table :subscriptions do |t|
      t.references :user, index: true
      t.references :roadmap, index: true
      t.boolean :is_subscribed, default: false, index: true

      t.timestamps
    end

    add_index :subscriptions, [:user_id, :roadmap_id], unique: true

    UserRoadmap.all.each do |ur|
      ur.init_user_subscription
    end
  end

  def down
    drop_table :subscriptions
  end
end
