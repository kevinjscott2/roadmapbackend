class CreateTimelines < ActiveRecord::Migration
  def change
    create_table :timelines do |t|
      t.string :title
      t.boolean :is_archive, default: false

      t.timestamps
    end
    add_index :timelines, :title, :unique => true
  end
end
