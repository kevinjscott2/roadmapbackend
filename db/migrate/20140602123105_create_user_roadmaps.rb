class CreateUserRoadmaps < ActiveRecord::Migration
  def change
    create_table :user_roadmaps do |t|
      t.references :user, index: true
      t.references :roadmap, index: true
      t.integer :rights

      t.timestamps
    end
  end
end
