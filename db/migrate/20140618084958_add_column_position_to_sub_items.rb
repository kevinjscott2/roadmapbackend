class AddColumnPositionToSubItems < ActiveRecord::Migration
  def change
    add_column :sub_items, :position, :integer
  end
end
