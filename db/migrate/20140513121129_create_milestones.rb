class CreateMilestones < ActiveRecord::Migration
  def change
    create_table :milestones do |t|
      t.text :description
      t.integer :timeline_id
      t.integer :label_id
      t.date :date_end

      t.timestamps
    end
  end
end
