class AddEntityDetailsToActivities < ActiveRecord::Migration
  def change
    add_column :activities, :entity_details, :text
    remove_column :activities, :entity_id
  end
end
