class AddRoadmapIdToLabels < ActiveRecord::Migration
  def change
    add_reference :labels, :roadmap, index: true
  end
end
