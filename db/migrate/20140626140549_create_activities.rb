class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.integer :entity_id, index: true
      t.string :entity_type, index: true
      t.text :deltas
      t.references :user, index: true
      t.references :roadmap, index: true

      t.timestamps
    end
  end
end
