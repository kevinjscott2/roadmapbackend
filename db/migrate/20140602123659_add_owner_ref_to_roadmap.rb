class AddOwnerRefToRoadmap < ActiveRecord::Migration
  def change
    add_reference :roadmaps, :owner, index: true
  end
end
