class CreateRoadmaps < ActiveRecord::Migration
  def change
    create_table :roadmaps do |t|
      t.string :name
      t.boolean :is_archive, default: false

      t.timestamps
    end
    add_index :roadmaps, :name
  end
end
