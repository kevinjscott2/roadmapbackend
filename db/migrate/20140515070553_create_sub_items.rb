class CreateSubItems < ActiveRecord::Migration
  def change
    create_table :sub_items do |t|
      t.string :name
      t.integer :milestone_id

      t.timestamps
    end
  end
end
