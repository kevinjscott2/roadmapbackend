class ChangeTitleIndexTypeInTimeline < ActiveRecord::Migration
  def self.down
    remove_index :timelines, :title
    add_index :timelines, :title, unique: true
  end

  def self.up
    remove_index :timelines, :title
    add_index :timelines, :title
  end
end
