class AddColumnIsPrivateToRoadmaps < ActiveRecord::Migration
  def change
    add_column :roadmaps, :is_private, :boolean
  end
end
