class AddColumnRoadmapIdToTimelines < ActiveRecord::Migration
  def change
    add_column :timelines, :roadmap_id, :integer
    add_index :timelines, :roadmap_id
  end
end
