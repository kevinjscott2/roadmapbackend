class AddColorIdToLabels < ActiveRecord::Migration
  def change
    add_column :labels, :color_id, :integer
  end
end
