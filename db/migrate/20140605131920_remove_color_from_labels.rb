class RemoveColorFromLabels < ActiveRecord::Migration
  def up
    remove_column :labels, :color, :string
  end

  def down
    add_column :labels, :color, :string
  end
end
