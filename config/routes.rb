Roadmapbackend::Application.routes.draw do

  root 'home#index'
  mount RoadMap::API => '/api'

  #if Rails.env.development?
  mount GrapeSwaggerRails::Engine => '/api_doc'
  #end
end