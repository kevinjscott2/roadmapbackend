set :stages, %w(staging, production)
set :default_stage, 'staging'

set :application, 'roadmapbackend'
set :repo_url, 'git@git.mlsdev.com:timkov/roadmapbackend.git'

set :scm, :git
set :scm_username, 'jenkins'

set :rvm_ruby_version, 'ruby-2.0.0@roadmapbackend'
set :rvm_type, :user
set :default_env, { rvm_bin_path: '~/.rvm/bin' }

set :linked_files, %w{config/database.yml config/unicorn.rb}
set :linked_dirs, %w{bin log tmp/pids tmp/sockets vendor/bundle public/system}


# which config files should be copied by deploy:setup_config
# see documentation in lib/capistrano/tasks/setup_config.cap
# for details of operations
#
# USAGE:
# DB_USERNAME=username DB_PASSWORD=password cap staging deploy:setup_config
set(:config_files, %w(
  database.yml
  unicorn.rb
))

set :keep_releases, 2

namespace :deploy do
  desc "reload the database with seed data"
  task :seed do
    run "cd #{release_path}; bundle exec rake db:seed RAILS_ENV=#{rails_env}"
  end

  after :finishing, 'unicorn:restart'
  after :publishing, 'delayed_job:restart'
end