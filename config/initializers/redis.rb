require 'redis'
$redis = Redis.new db: 15, driver: :hiredis
if !Rails.env.test?
  $redis.client.reconnect
end