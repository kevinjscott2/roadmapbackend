def in_memory_database?
  Rails.env == 'test' &&
      ActiveRecord::Base.connection.class == ActiveRecord::ConnectionAdapters::SQLite3Adapter &&
      Rails.configuration.database_configuration['test']['database'] == ':memory:'
end

#if in_memory_database?
#  puts 'creating sqlite in memory database'
#  load "#{Rails.root.to_s}/db/schema.rb"
#end
