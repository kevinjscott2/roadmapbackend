set :branch, 'develop'

set :deploy_to, '/var/www/roadmapbackend-staging'

set :database_name, 'roadmapbackend_develop'
set :rvm_ruby_version, 'ruby-2.0.0@roadmapbackend'
set :unicorn_pid, "#{deploy_to}/current/tmp/pids/unicorn.pid"
set :unicorn_conf, "#{deploy_to}/current/config/unicorn.rb"

set :rails_env, :development

server 'roadmapbackend-staging.mlsdev.com', user: 'www-data', roles: %w{web app db}

set :keep_releases, 3