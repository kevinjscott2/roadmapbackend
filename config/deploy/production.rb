set :branch, 'master'

set :deploy_to, '/var/www/html/roadmap-backend'

set :database_name, 'roadmapbackend'
set :rvm_ruby_version, 'ruby-2.0.0@roadmapbackend'
set :unicorn_pid, "#{deploy_to}/current/tmp/pids/unicorn.pid"
set :unicorn_conf, "#{deploy_to}/current/config/unicorn.rb"

set :rails_env, :production

server 'backend.roadmap.espninnovation.com', user: 'nginx', roles: %w{web app db}

set :keep_releases, 3