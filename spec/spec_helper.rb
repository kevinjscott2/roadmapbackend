# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)

if in_memory_database?
  puts 'creating sqlite in memory database'
  load "#{Rails.root.to_s}/db/schema.rb"
end

require 'rspec/rails'
require 'database_cleaner'
require 'fileutils'
require 'paperclip/matchers'

# Requires supporting ruby files with custom matchers and macros, etc, in
# spec/support/ and its subdirectories. Files matching `spec/**/*_spec.rb` are
# run as spec files by default. This means that files in spec/support that end
# in _spec.rb will both be required and run as specs, causing the specs to be
# run twice. It is recommended that you do not name files matching this glob to
# end with _spec.rb. You can configure this pattern with with the --pattern
# option on the command line or in ~/.rspec, .rspec or `.rspec-local`.
Dir[Rails.root.join("spec/support/**/*.rb")].each { |f| require f }

# Checks for pending migrations before tests are run.
# If you are not using ActiveRecord, you can remove this line.
ActiveRecord::Migration.check_pending! if defined?(ActiveRecord::Migration)
RSpec.configure do |config|
  # ## Mock Framework
  #
  # If you prefer to use mocha, flexmock or RR, uncomment the appropriate line:
  #
  # config.mock_with :mocha
  # config.mock_with :flexmock
  # config.mock_with :rr
  config.include Paperclip::Shoulda::Matchers

  config.before(:suite) do
    DatabaseCleaner.strategy = :truncation

    Role.create([{ :name => 'Admin' }, { :name => 'Member' }])
    FactoryGirl.create(:user_with_roadmaps, user_role: :Admin)
    FactoryGirl.create(:user_with_roadmaps, user_role: :Member)
    FactoryGirl.create(:user_with_roadmaps, user_role: :Member, roadmap_count: 1, roadmap_is_private: false)
  end

  config.before(:all) do
  end

  config.after(:all) do # or :each or :all
    FileUtils.remove_dir "#{Rails.root}/public/assets/test", true
  end

  config.after(:suite) do
    DatabaseCleaner.clean
  end


  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = true

  # Run specs in random order to surface order dependencies. If you find an
  # order dependency and want to debug it, you can fix the order by providing
  # the seed, which is printed after each run.
  #     --seed 1234
  config.order = "random"

  config.expect_with :rspec do |c|
    c.syntax = :expect             # disables `should`
  end
  config.include Requests::JsonHelpers, :type => :request
end
