shared_examples 'a created request' do
  it 'respond with 201' do
    expect(response.status).to eq(201)
  end
end

shared_examples 'a successful request' do
  it 'respond with 200' do
    expect(response.status).to eq(200)
  end
end

shared_examples 'a bad request' do
  it 'respond with 400' do
    expect(response.status).to eq(400)
  end
end


shared_examples 'a unauthorized request' do
  it 'respond with 401' do
    expect(response.status).to eq(401)
  end
end

shared_examples 'a forbidden request' do
  it 'respond with 403' do
    expect(response.status).to eq(403)
  end
end

shared_examples 'not found request' do
  it 'respond with 404' do
    expect(response.status).to eq(404)
  end
end

shared_examples 'an unprocessable entity request' do
  it 'respond with 422' do
    expect(response.status).to eq(422)
  end
end

# shared_examples 'model activity' do |activity_type|
#   describe '' do
#     let(:last_activity) { Activity.order(id: :asc).last }
#
#     it 'for proper entity' do
#       expect(last_activity.entity_details[:id]).to eq entity.id
#       expect(last_activity.entity_type).to eq entity.class.name
#       expect(last_activity.user_id).to eq activity_author.id
#     end
#
#     it "for #{activity_type.to_s.upcase} action" do
#       case activity_type
#         when :create
#           expect(last_activity.deltas).to have_key 'id'
#           expect(last_activity.deltas['id']).to eq [nil, entity.id]
#         when :update
#           expect(last_activity.deltas).not_to have_key 'id'
#         when :destroy
#           expect(last_activity.deltas).to have_key 'id'
#           expect(last_activity.deltas['id']).to eq [entity.id, nil]
#         when :copy
#           expect(last_activity.deltas.keys).to eq [:self, :roadmap]
#         else
#           raise ArgumentError, 'Unknown activity type'
#       end
#     end
#   end
# end