shared_examples 'model activity' do |activity_type|
  describe '' do
    let(:last_activity) { Activity.order(id: :asc).last }

    it 'for proper entity' do
      expect(last_activity.entity_details[:id]).to eq entity.id
      expect(last_activity.entity_type).to eq entity.class.name
      expect(last_activity.user_id).to eq activity_author.id
    end

    it "for #{activity_type.to_s.upcase} action" do
      case activity_type
        when :create
          expect(last_activity.deltas).to have_key 'id'
          expect(last_activity.deltas['id']).to eq [nil, entity.id]
        when :update
          expect(last_activity.deltas).not_to have_key 'id'
        when :destroy
          expect(last_activity.deltas).to have_key 'id'
          expect(last_activity.deltas['id']).to eq [entity.id, nil]
        when :copy
          expect(last_activity.deltas.keys).to eq [:self, :roadmap]
        else
          raise ArgumentError, 'Unknown activity type'
      end
    end
  end
end