FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "admi#{n}@test.test" }
    sequence(:token) { |n| "74578vy87v548bv7359t4#{n}" }
    avatar  File.new(Rails.root + 'spec/attachments/avatar.png')

    factory :user_without_token, parent: :user do
      before(:create) do |user|
        user.token = ''
      end
    end

    factory :admin, parent: :user do
      after(:create) do |user|
        user.add_role(:admin)
      end
    end

    factory :member, parent: :user do
      after(:create) do |user|
        user.add_role(:member)
      end
    end

    factory :user_with_roadmaps, parent: :user do
      ignore do
        roadmap_count 5
        user_role :Admin
        roadmap_is_private true
        roadmap_factory :roadmap_with_timelines
      end
      before(:create) do |user, evalutor|
        user.add_role(evalutor.user_role)
      end

      after(:create) do |user, evaluator|
        create_list(evaluator.roadmap_factory, evaluator.roadmap_count, user: user, is_private: evaluator.roadmap_is_private)
      end
    end
  end
end