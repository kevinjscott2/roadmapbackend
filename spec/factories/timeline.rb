FactoryGirl.define do
  factory :timeline do
    sequence(:title) { |n| "title #{n}" }
    sequence(:is_archive) { |n| n.odd? ? true : false }

    factory :timeline_with_milestones, parent: :timeline do
      ignore do
        milestones_count 4
      end

      after(:create) do |timeline, evaluator|
        create_list(:milestone_full, evaluator.milestones_count, timeline: timeline,
              label: timeline.roadmap.labels[rand(6)])
      end
    end
  end
end