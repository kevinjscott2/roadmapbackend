FactoryGirl.define do
  factory :roadmap do
    sequence(:name) { |n| "name #{n}" }
    sequence(:is_archive) { |n| n % 3 == 0 ? true : false }

    ignore do
      user nil
    end

    before(:create) do |roadmap, evaluator|
      roadmap.owner = evaluator.user
    end

    after(:create) do |roadmap, evaluator|
      FactoryGirl.create(:user_roadmap, user: evaluator.user, roadmap: roadmap)
    end

    factory :roadmap_with_timelines, parent: :roadmap do
      ignore do
        timelines_count 5
      end

      after(:create) do |roadmap, evaluator|
        create_list(:timeline_with_milestones, evaluator.timelines_count, roadmap: roadmap)
      end
    end
  end
end