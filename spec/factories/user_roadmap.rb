FactoryGirl.define do
  factory :user_roadmap do
    sequence(:rights) { |n| n % 2 }
  end
end