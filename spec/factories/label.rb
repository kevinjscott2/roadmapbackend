FactoryGirl.define do
  factory :label do
    sequence(:text) { |n| "label #{n}" }
    color_id 1
    roadmap_id 1
  end
end