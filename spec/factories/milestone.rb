FactoryGirl.define do
  factory :milestone do
    sequence(:description) { |n| "text #{n}" }
    date_end Date.today

    factory :milestone_full, parent: :milestone do
      ignore do
        sub_items_count 2
      end

      after(:create) do |milestone, evaluator|
        create_list(:sub_item, evaluator.sub_items_count, milestone: milestone)
      end
    end
  end
end