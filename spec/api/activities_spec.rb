require 'spec_helper'

describe 'RoadMap::API::Activities ' do
  let(:admin) { User.joins(:roles).where(roles: { name: :Admin }).first }
  let(:member) { User.joins(:roles).where(roles: { name: :Member }).first }
  let(:public_roadmap) { Roadmap.where(is_private: false).first }
  let(:private_member_r_roadmap) {
    Roadmap.archive.joins(:user_roadmaps).where(is_private: true)
    .where(user_roadmaps: { user: member, rights: UserRoadmap::USER_RIGHTS[:reader] }).first
  }
  let(:private_member_a_roadmap) {
    Roadmap.archive.joins(:user_roadmaps).where(is_private: true)
    .where(user_roadmaps: { user: member, rights: UserRoadmap::USER_RIGHTS[:admin] }).first
  }
  let(:private_admin_roadmap) {
    Roadmap.archive.where(is_private: true, owner: admin).first
  }

  describe 'GET /' do

    context 'when send wrong hash' do
      before { get '/api/v1/roadmaps', { hash: 'wrong hash' }}

      it_behaves_like 'not found request'
    end

    context 'when roadmap public' do
      before { get '/api/v1/activities', { roadmap_id: public_roadmap.id }}

      it_behaves_like 'a successful request'

      # TODO: testing structure
    end

    context 'when roadmap private' do
      context 'when user admin' do
        before { get '/api/v1/activities', { roadmap_id: private_member_r_roadmap.id, token: admin.token }}

        it_behaves_like 'a successful request'
      end

      context 'when user member' do
        context 'with admin, reader rights' do
          before { get '/api/v1/activities', { roadmap_id: private_member_r_roadmap.id, token: member.token }}

          it_behaves_like 'a successful request'
        end

        context 'with no rights' do
          before { get '/api/v1/activities', { roadmap_id: private_admin_roadmap.id, token: member.token }}

          it_behaves_like 'a forbidden request'
        end

        context 'with invalid token' do
          before { get '/api/v1/activities', { roadmap_id: private_admin_roadmap.id, token: 'invalid token' }}

          it_behaves_like 'a unauthorized request'
        end
      end
    end
  end
end
