require 'spec_helper'

describe 'RoadMap::API::Timelines' do
  let(:admin) { User.joins(:roles).where(roles: { name: :Admin }).first }
  let(:member) { User.joins(:roles).where(roles: { name: :Member }).first }
  let(:public_roadmap) { Roadmap.where(is_private: false).first }
  let(:private_member_r_roadmap) {
    Roadmap.joins(:user_roadmaps)
      .where(is_private: true, user_roadmaps: { user: member, rights: UserRoadmap::USER_RIGHTS[:reader] }).first
  }
  let(:private_member_a_roadmap) {
    Roadmap.joins(:user_roadmaps)
    .where(is_private: true, user_roadmaps: { user: member, rights: UserRoadmap::USER_RIGHTS[:admin] }).first
  }
  let(:private_admin_roadmap) {
    Roadmap.where(is_private: true, owner: admin).first
  }

  describe 'GET /' do
    context 'with invalid token' do
      before { get '/api/v1/timelines/', { token: 'invalid token', roadmap_id: private_admin_roadmap.id }}

      it_behaves_like 'a unauthorized request'
    end

    context 'when roadmap not found' do
      before { get '/api/v1/timelines/', { roadmap_id: 65656 }}

      it_behaves_like 'not found request'
    end

    context 'when user admin' do
      before { get '/api/v1/timelines/', {
        token: admin.token,
        roadmap_id: private_admin_roadmap.id
      }}

      it_behaves_like 'a successful request'

      it 'has correct structure' do
        expect(json).to be_a_kind_of(Array)
        expect(json.first).to have_key('id')
        expect(json.first).to have_key('title')
      end

      it 'has correct count' do
        expect(json.count).to eq Timeline.where(roadmap_id: private_admin_roadmap.id).count
      end
    end

    context 'when related roadmap public' do
      before { get '/api/v1/timelines/', { roadmap_id: public_roadmap.id }}

      it_behaves_like 'a successful request'
    end

    context 'when related roadmap private' do
      context 'when user member' do
        context 'with admin, reader rights' do
          before { get '/api/v1/timelines/', {
            roadmap_id: private_member_r_roadmap.id,
            token: member.token
          }}

          it_behaves_like 'a successful request'
        end

        context 'without rights' do
          before { get '/api/v1/timelines/', {
            roadmap_id: private_admin_roadmap.id,
            token: member.token
          }}

          it_behaves_like 'a forbidden request'
        end
      end
    end
  end


  describe 'POST /' do
    context 'when invalid data send' do
      before { post '/api/v1/timelines/', { token: admin.token, title: '', roadmap_id: Roadmap.first.id } }

      it_behaves_like 'an unprocessable entity request'
    end

    context 'when user admin' do
      before { post '/api/v1/timelines/', {
        token: admin.token,
        title: 'some title',
        roadmap_id: private_admin_roadmap.id
      }}

      it_behaves_like 'a created request'

      it 'has correct structure' do
        expect(json).to have_key('id')
      end
    end

    context 'when user member' do
      context 'with admin rights' do
        before { post '/api/v1/timelines/', {
          token: member.token,
          title: 'some title',
          roadmap_id: private_member_a_roadmap.id
        }}

        it_behaves_like 'a created request'
      end

      context 'with reader rights' do
        before { post '/api/v1/timelines/', {
            token: member.token,
            title: 'some title',
            roadmap_id: private_member_r_roadmap.id
        }}

        it_behaves_like 'a forbidden request'
      end

      context 'without rights' do
        before { post '/api/v1/timelines/', {
          token: member.token,
          title: 'some title',
          roadmap_id: private_admin_roadmap.id
        }}

        it_behaves_like 'a forbidden request'
      end
    end

    context 'when token invalid' do
      before { post '/api/v1/timelines', {
        token: 'invalid token',
        title: 'some title',
        roadmap_id: private_admin_roadmap.id
      }}

      it_behaves_like 'a unauthorized request'
    end
  end


  describe 'PUT /:id' do
    context 'when timeline not found' do
      before { put '/api/v1/timelines/9999', {
        token: admin.token,
        title: '123',
        roadmap_id: private_member_a_roadmap.id,
        is_archive: true
      }}

      it_behaves_like 'not found request'
    end

    context 'when invalid data send' do
      before { put '/api/v1/timelines/' + private_admin_roadmap.timelines.first.id.to_s, {
        token: admin.token,
        title: '',
        is_archive: true,
        roadmap_id: Roadmap.first.id
      }}

      it_behaves_like 'an unprocessable entity request'
    end

    context 'when token invalid' do
      before { put '/api/v1/timelines/' + private_admin_roadmap.timelines.first.id.to_s, {
        token: 'invalid token',
        title: '123',
        roadmap_id: Roadmap.first.id,
        is_archive: true
      }}

      it_behaves_like 'a unauthorized request'
    end

    context 'when user admin' do
      before { put '/api/v1/timelines/' + private_admin_roadmap.timelines.first.id.to_s, {
        token: admin.token,
        title: '123',
        roadmap_id: private_admin_roadmap.id,
        is_archive: true
      }}

      it_behaves_like 'a successful request'

      it 'has correct structure' do
        expect(json).to have_key('id')
      end
    end

    context 'when user member' do
      context 'with admin rights' do
        before { put '/api/v1/timelines/' + private_member_a_roadmap.timelines.first.id.to_s, {
          token: member.token,
          title: '123',
          roadmap_id: private_member_a_roadmap.id,
          is_archive: true
        }}

        it_behaves_like 'a successful request'
      end

      context 'with reader rights' do
        before { put '/api/v1/timelines/' + private_member_r_roadmap.timelines.first.id.to_s, {
            token: member.token,
            title: '123',
            roadmap_id: private_member_r_roadmap.id,
            is_archive: true
        }}

        it_behaves_like 'a forbidden request'
      end

      context 'without rights' do
        before {
          put '/api/v1/timelines/' + private_admin_roadmap.timelines.first.id.to_s, {
            token: member.token,
            title: 'some title',
            roadmap_id:private_admin_roadmap.id,
            is_archive: false
          }}

        it_behaves_like 'a forbidden request'
      end
    end
  end

  describe 'Delete /:id' do
    context 'when timeline not found' do
      before { delete '/api/v1/timelines/555', {
        token: admin.token
      }}

      it_behaves_like 'not found request'
    end

    context 'when timeline not archive' do
      before { delete '/api/v1/timelines/' + private_admin_roadmap.timelines.active.first.id.to_s, {
        token: admin.token
      }}

      it_behaves_like 'not found request'
    end

    context 'when token invalid' do
      before { delete '/api/v1/timelines/' + private_admin_roadmap.timelines.first.id.to_s, {
        token: 'invalid token'
      }}

      it_behaves_like 'a unauthorized request'
    end

    context 'when user admin' do
      let(:timeline) { private_admin_roadmap.timelines.archive.first }
      before { delete '/api/v1/timelines/' + timeline.id.to_s, {
        token: admin.token
      }}

      it_behaves_like 'a successful request'

      it 'has correct response structure' do
        expect(json).to have_key('id')
      end
    end

    context 'when user member' do
      context 'with admin rights' do
        before { delete '/api/v1/timelines/' + private_member_a_roadmap.timelines.archive.first.id.to_s, {
          token: member.token,
        }}

        it_behaves_like 'a successful request'
      end

      context 'with reader rights' do
        before { delete '/api/v1/timelines/' + private_member_r_roadmap.timelines.archive.first.id.to_s, {
            token: member.token,
        }}

        it_behaves_like 'a forbidden request'
      end

      context 'without rights' do
        before { delete '/api/v1/timelines/' + private_admin_roadmap.timelines.archive.first.id.to_s, {
          token: member.token,
        }}

        it_behaves_like 'a forbidden request'
      end
    end
  end

  describe 'POST /:id/copy' do
    let(:timeline) { private_admin_roadmap.timelines.first }
    let(:copy_timeline_url) { "/api/v1/timelines/#{timeline.id}/copy" }

    context 'when timeline not found' do
      before { post '/api/v1/timelines/9999/copy', {
          token: admin.token,
          roadmap_id: Roadmap.first.id
      }}

      it_behaves_like 'not found request'
    end

    context 'when destination roadmap is not exists' do
      before { post copy_timeline_url, {
          token: admin.token,
          roadmap_id: 9999
      }}

      it_behaves_like 'not found request'
    end

    context 'when token invalid' do
      before { post copy_timeline_url, {
          token: 'invalid token',
          roadmap_id: Roadmap.first.id
      }}

      it_behaves_like 'a unauthorized request'
    end

    context 'when user admin' do
      before { post copy_timeline_url, {
          token: admin.token,
          roadmap_id: private_admin_roadmap.id
      }}

      it_behaves_like 'a created request'

      it 'has correct structure' do
        expect(json).to have_key('id')
      end
    end

    context 'when user member' do
      #context 'with admin rights' do
      #  before { post copy_timeline_url, {
      #      token: member.token,
      #      roadmap_id: private_member_a_roadmap.id
      #  }}
      #
      #  it_behaves_like 'a created request'
      #
      #  it 'has correct structure' do
      #    expect(json).to have_key('id')
      #  end
      #end

      context 'with reader rights' do
        before { post copy_timeline_url, {
            token: member.token,
            roadmap_id: private_member_r_roadmap.id
        }}

        it_behaves_like 'a forbidden request'
      end

      context 'without rights' do
        before {
          post copy_timeline_url, {
              token: member.token,
              roadmap_id:private_admin_roadmap.id
          }}

        it_behaves_like 'a forbidden request'
      end
    end
  end


  describe 'PUT /:id/move' do
    let(:timeline) { private_admin_roadmap.timelines.first }
    let(:move_timeline_url) { "/api/v1/timelines/#{timeline.id}/move" }

    context 'when timeline not found' do
      before { put '/api/v1/timelines/9999/move', {
          token: admin.token,
          roadmap_id: Roadmap.first.id
      }}

      it_behaves_like 'not found request'
    end

    context 'when destination roadmap is not exists' do
      before { put move_timeline_url, {
          token: admin.token,
          roadmap_id: 9999
      }}

      it_behaves_like 'not found request'
    end

    context 'when token invalid' do
      before { put move_timeline_url, {
          token: 'invalid token',
          roadmap_id: Roadmap.first.id
      }}

      it_behaves_like 'a unauthorized request'
    end

    context 'when user admin' do
      before { put move_timeline_url, {
          token: admin.token,
          roadmap_id: private_admin_roadmap.id
      }}

      it_behaves_like 'a successful request'

      it 'has correct structure' do
        expect(json).to have_key('id')
      end
    end

    context 'when user member' do
      #context 'with admin rights' do
      #  before { put move_timeline_url, {
      #      token: member.token,
      #      roadmap_id: private_member_a_roadmap.id
      #  }}
      #
      #  it_behaves_like 'a successful request'
      #
      #  it 'has correct structure' do
      #    expect(json).to have_key('id')
      #  end
      #end

      context 'with reader rights' do
        before { put move_timeline_url, {
            token: member.token,
            roadmap_id: private_member_r_roadmap.id
        }}

        it_behaves_like 'a forbidden request'
      end

      context 'without rights' do
        before {
          put move_timeline_url, {
              token: member.token,
              roadmap_id:private_admin_roadmap.id
          }}

        it_behaves_like 'a forbidden request'
      end
    end
  end

end