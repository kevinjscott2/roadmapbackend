require 'spec_helper'

describe 'RoadMap::API::Subscriptions' do
  let(:admin) { User.joins(:roles).where(roles: { name: :Admin }).first }
  let(:member) { User.joins(:roles).where(roles: { name: :Member }).first }
  let(:public_roadmap) { Roadmap.where(is_private: false).first }
  let(:private_admin_roadmap) {
    Roadmap.archive.where(is_private: true, owner: admin).first
  }
  let(:subscriptions_url) { '/api/v1/subscriptions' }

  describe 'POST /' do
    context 'when token invalid' do
      before { post subscriptions_url, {
          token: 'invalid token',
          roadmap_id: public_roadmap.id
      }}

      it_behaves_like 'a unauthorized request'
    end

    context 'when user admin' do
      before { post subscriptions_url, {
          token: admin.token,
          roadmap_id: private_admin_roadmap.id
      }}

      it_behaves_like 'a created request'

      it 'has correct structure' do
        expect(json).to have_key('roadmap_id')
        expect(json['roadmap_id']).to eq private_admin_roadmap.id
        expect(json).to have_key('is_subscribed')
        expect(json['is_subscribed']).to eq true
      end
    end

    context 'when user member' do
      context 'without rights' do
        before { post subscriptions_url, {
            token: member.token,
            roadmap_id: private_admin_roadmap.id
        }}

        it_behaves_like 'a forbidden request'
      end
    end
  end

  describe 'GET /' do
    context 'when token invalid' do
      before { get subscriptions_url, { token: 'invalid token' }}

      it_behaves_like 'a unauthorized request'
    end

    context 'when token valid' do
      before { get subscriptions_url, {
          token: admin.token
      }}

      it_behaves_like 'a successful request'

      it 'has correct structure' do
        expect(json).to be_a Array
        expect(json.first).to have_key 'roadmap_id'
        expect(json.first).to have_key 'is_subscribed'
      end
    end
  end


  describe 'DELETE /' do
    context 'when token invalid' do
      before { delete subscriptions_url, { token: 'invalid token', roadmap_id: public_roadmap.id }}

      it_behaves_like 'a unauthorized request'
    end

    context 'when user admin' do
      before { delete subscriptions_url, { token: admin.token, roadmap_id: private_admin_roadmap.id }}

      it_behaves_like 'a successful request'

      it 'has correct response structure' do
        expect(json).to have_key 'roadmap_id'
        expect(json['roadmap_id']).to be private_admin_roadmap.id
        expect(json).to have_key 'is_subscribed'
        expect(json['is_subscribed']).to be false
      end
    end

    context 'when user member' do
      context 'without rights' do
        before { delete subscriptions_url, { token: member.token, roadmap_id: private_admin_roadmap.id }}

        it_behaves_like 'a forbidden request'
      end
    end
  end
end