require 'spec_helper'

describe 'RoadMap::API::Roadmaps' do
  let(:admin) { User.joins(:roles).where(roles: { name: :Admin }).first }
  let(:member) { User.joins(:roles).where(roles: { name: :Member }).first }
  let(:public_roadmap) { Roadmap.where(is_private: false).first }
  let(:private_member_r_roadmap) {
    Roadmap.archive.joins(:user_roadmaps).where(is_private: true)
              .where(user_roadmaps: { user: member, rights: UserRoadmap::USER_RIGHTS[:reader] }).first
  }
  let(:private_member_a_roadmap) {
    Roadmap.archive.joins(:user_roadmaps).where(is_private: true)
    .where(user_roadmaps: { user: member, rights: UserRoadmap::USER_RIGHTS[:admin] }).first
  }
  let(:private_admin_roadmap) {
    Roadmap.archive.where(is_private: true, owner: admin).first
  }

  describe 'GET /:hash' do
    context 'when send wrong hash' do
      before { get '/api/v1/roadmaps', { hash: 'wrong hash' }}

      it_behaves_like 'not found request'
    end

    context 'when roadmap public' do
      before { get '/api/v1/roadmaps', { hash: public_roadmap.hashed_id }}

      it_behaves_like 'a successful request'

      it 'has correct structure' do
        expect(json).to be_a_kind_of(Hash)
        expect(json).to have_key('id')
        expect(json).to have_key('name')
        expect(json).to have_key('hash')
      end
    end

    context 'when roadmap private' do
      context 'when user admin' do
        before { get '/api/v1/roadmaps', { hash: private_member_r_roadmap.hashed_id, token: admin.token }}

        it_behaves_like 'a successful request'
      end

      context 'when user member' do
        context 'with admin, reader rights' do
          before { get '/api/v1/roadmaps', { hash: private_member_r_roadmap.hashed_id, token: member.token }}

          it_behaves_like 'a successful request'
        end

        context 'with no rights' do
          before { get '/api/v1/roadmaps', { hash: private_admin_roadmap.hashed_id, token: member.token }}

          it_behaves_like 'a forbidden request'
        end

        context 'with invalid token' do
          before { get '/api/v1/roadmaps', { hash: private_admin_roadmap.hashed_id, token: 'invalid token' }}

          it_behaves_like 'a unauthorized request'
        end
      end
    end
  end


  describe 'GET /by_milestone/:milestone_hash' do
    context 'when send wrong hash' do
      before { get '/api/v1/roadmaps/by_milestone', { milestone_hash: 'wrong milestone hash' }}

      it_behaves_like 'not found request'
    end

    context 'when roadmap public' do
      before { get '/api/v1/roadmaps/by_milestone', {
        milestone_hash: public_roadmap.timelines.first.milestones.first.hashed_id
      }}

      it_behaves_like 'a successful request'

      it 'has correct structure' do
        expect(json).to be_a_kind_of(Hash)
        expect(json).to have_key('id')
        expect(json).to have_key('name')
        expect(json).to have_key('hash')
      end
    end

    context 'when roadmap private' do
      context 'when user admin' do
        before { get '/api/v1/roadmaps/by_milestone', {
          milestone_hash: private_member_r_roadmap.timelines.first.milestones.first.hashed_id,
          token: admin.token
        }}

        it_behaves_like 'a successful request'
      end

      context 'when user member' do
        context 'with admin, reader rights' do
          before { get '/api/v1/roadmaps/by_milestone', {
            milestone_hash: private_member_r_roadmap.timelines.first.milestones.first.hashed_id,
            token: member.token
          }}

          it_behaves_like 'a successful request'
        end
        context 'with no rights' do
          before { get '/api/v1/roadmaps/by_milestone', {
            milestone_hash: private_admin_roadmap.timelines.first.milestones.first.hashed_id,
            token: member.token
          }}

          it_behaves_like 'a forbidden request'
        end
        context 'with invalid token' do
          before { get '/api/v1/roadmaps/by_milestone', {
            milestone_hash: public_roadmap.timelines.first.milestones.first.hashed_id,
            token: 'invalid token'
          }}

          it_behaves_like 'a unauthorized request'
        end
      end
    end
  end


  describe 'GET /active' do
    describe 'return paginated data' do
      before { get '/api/v1/roadmaps/active', { token: admin.token, page: 1, per_page: 3 }}
      it 'has correct count' do
        expect(json.count).to eq 3
      end
    end

    context 'with invalid token' do
      before { get '/api/v1/roadmaps/active', { token: 'invalid token' }}

      it_behaves_like 'a unauthorized request'
    end

    context 'when user is admin' do
      before { get '/api/v1/roadmaps/active', { token: admin.token }}

      it_behaves_like 'a successful request'

      it 'has correct structure' do
        expect(json).to be_a_kind_of(Array)
        expect(json.first).to have_key('id')
        expect(json.first).to have_key('name')
        expect(json.first).to have_key('hash')
        expect(json.first).to have_key('is_private')
      end

      it 'has correct count' do
        expect(json.count).to eq Roadmap.active.get_available(admin).count
      end
    end

    context 'when user is member' do
      before { get '/api/v1/roadmaps/active', { token: member.token }}

      it_behaves_like 'a successful request'

      it 'has correct structure' do
        expect(json).to be_a_kind_of(Array)
        expect(json.first).to have_key('id')
        expect(json.first).to have_key('name')
        expect(json.first).to have_key('hash')
        expect(json.first).to have_key('is_private')
      end

      it 'has correct count' do
        expect(json.count).to eq Roadmap.active.get_available(member).count
      end
    end
  end


  describe 'GET /archive' do
    describe 'return paginated data' do
      before { get '/api/v1/roadmaps/archive', { token: admin.token, page: 1, per_page: 3 }}
      it 'has correct count' do
        expect(json.count).to eq 3
      end
    end

    context 'with invalid token' do
      before { get '/api/v1/roadmaps/archive', {
          token: 'invalid token'
      }}

      it_behaves_like 'a unauthorized request'
    end

    context 'when user is admin' do
      before { get '/api/v1/roadmaps/archive', { token: admin.token }}

      it_behaves_like 'a successful request'

      it 'has correct structure' do
        expect(json).to be_a_kind_of(Array)
        expect(json.first).to have_key('id')
        expect(json.first).to have_key('name')
        expect(json.first).to have_key('hash')
        expect(json.first).to have_key('is_private')
      end

      it 'has correct count' do
        expect(json.count).to eq Roadmap.archive.get_available(admin).count
      end
    end

    context 'when user is member' do
      before { get '/api/v1/roadmaps/archive', { token: member.token }}

      it_behaves_like 'a successful request'

      it 'has correct structure' do
        expect(json).to be_a_kind_of(Array)
        expect(json.first).to have_key('id')
        expect(json.first).to have_key('name')
        expect(json.first).to have_key('hash')
        expect(json.first).to have_key('is_private')
      end

      it 'has correct count' do
        expect(json.count).to eq Roadmap.archive.get_available(member).count
      end
    end
  end


  describe 'POST /' do
    context 'when invalid data send' do
      before { post '/api/v1/roadmaps/', { token: admin.token, name: '' }}

      it_behaves_like 'an unprocessable entity request'
    end

    context 'when token valid' do
      before { post '/api/v1/roadmaps/', { token: admin.token, name: 'some name' }}

      it_behaves_like 'a created request'

      it 'has correct structure' do
        expect(json).to have_key('id')
        expect(json).to have_key('hash')
      end

      it 'seed roadmap with test data' do
        roadmap = Roadmap.includes(timelines: [milestones: [:sub_items]]).last
        timeline = roadmap.timelines.first
        milestone1 = timeline.milestones.first
        milestone2 = timeline.milestones.second
        sub_items = milestone2.sub_items
        expect(timeline.title).to eq 'This is sample timeline'
        expect(milestone1.description).to eq 'My milestone'
        expect(milestone1.date_end).to eq 1.week.ago.to_date
        expect(milestone2.description).to eq 'A different milestone'
        expect(milestone2.date_end).to eq 1.week.ago.to_date
        expect(sub_items.length).to eq 3
      end
    end

    context 'when token invalid' do
      before { post '/api/v1/roadmaps', { token: 'invalid token', name: 'some name' }}

      it_behaves_like 'a unauthorized request'
    end
  end


  describe 'PUT /:id' do
    context 'when roadmap not found' do
      before { put '/api/v1/roadmaps/9999', {
        token: admin.token,
        name: '123',
        is_archive: true,
        is_private: false
      }}

      it_behaves_like 'not found request'
    end

    context 'when user is admin' do
      context 'when token invalid' do
        before { put '/api/v1/roadmaps/' + private_admin_roadmap.id.to_s, {
          token: 'invalid token',
          name: '123',
          is_archive: true,
          is_private: false
        }}

        it_behaves_like 'a unauthorized request'
      end

      context 'with invalid data send' do
        before { put '/api/v1/roadmaps/' + private_admin_roadmap.id.to_s, {
          token: admin.token,
          name: '',
          is_archive: true,
          is_private: false
        }}

        it_behaves_like 'an unprocessable entity request'
      end

      context 'with token valid' do
        before { put '/api/v1/roadmaps/' + private_admin_roadmap.id.to_s, {
          token: admin.token,
          name: '123',
          is_archive: true,
          is_private: false
        }}

        it_behaves_like 'a successful request'

        it 'has correct structure' do
          expect(json).to have_key('id')
        end
      end
    end

    context 'when user is member' do
      context 'with admin rights' do
        before { put '/api/v1/roadmaps/' + private_member_a_roadmap.id.to_s, {
          token: member.token,
          name: '123',
          is_archive: true,
          is_private: false
        }}

        it_behaves_like 'a successful request'
      end

      context 'with reader rights' do
        before { put '/api/v1/roadmaps/' + private_member_r_roadmap.id.to_s, {
          token: member.token,
          name: '123',
          is_archive: true,
          is_private: false
        }}

        it_behaves_like 'a forbidden request'
      end

      context 'with no rights' do
        before { put '/api/v1/roadmaps/' + private_admin_roadmap.id.to_s, {
          token: member.token,
          name: '123',
          is_archive: true,
          is_private: false
        }}

        it_behaves_like 'a forbidden request'
      end
    end
  end


  describe 'Delete /:id' do
    context 'when roadmap by id not found' do
      before { delete '/api/v1/roadmaps/555', { token: admin.token }}

      it_behaves_like 'not found request'
    end

    context 'when not archive' do
      before { delete '/api/v1/roadmaps/'  + Roadmap.active.first.id.to_s, { token: admin.token }}

      it_behaves_like 'not found request'
    end

    context 'when archive' do
      before { delete '/api/v1/roadmaps/' + private_admin_roadmap.id.to_s, { token: admin.token }}

      it_behaves_like 'a successful request'

      it 'has correct response structure' do
        expect(json).to have_key('id')
      end
    end

    context 'when token invalid' do
      before { delete '/api/v1/roadmaps/' + private_admin_roadmap.id.to_s, { token: 'invalid token' }}

      it_behaves_like 'a unauthorized request'
    end

    context 'when user member' do
      context 'with admin rights' do
        before {
          delete '/api/v1/roadmaps/' + private_member_a_roadmap.id.to_s, { token: member.token }
        }

        it_behaves_like 'a successful request'
      end

      context 'with reader rights' do
        before { delete '/api/v1/roadmaps/' + private_member_r_roadmap.id.to_s, { token: member.token }}

        it_behaves_like 'a forbidden request'
      end

      context 'with no rights' do
        before { delete '/api/v1/roadmaps/' + private_admin_roadmap.id.to_s, { token: member.token }}

        it_behaves_like 'a forbidden request'
      end
    end
  end
end