require 'spec_helper'

describe 'RoadMap::API::Milestones ' do
  let!(:admin) { User.joins(:roles).where(roles: { name: :Admin }).first }
  let(:member) { User.joins(:roles).where(roles: { name: :Member }).first }
  let(:milestone_data) { FactoryGirl.build(:milestone) }
  let(:public_roadmap) { Roadmap.where(is_private: false).first }
  let(:private_member_r_roadmap) {
    Roadmap.joins(:user_roadmaps)
    .where(is_private: true, user_roadmaps: { user: member, rights: UserRoadmap::USER_RIGHTS[:reader] }).first
  }
  let(:private_member_a_roadmap) {
    Roadmap.joins(:user_roadmaps)
    .where(is_private: true, user_roadmaps: { user: member, rights: UserRoadmap::USER_RIGHTS[:admin] }).first
  }
  let(:private_admin_roadmap) {
    Roadmap.where(is_private: true, owner: admin).first
  }

  describe 'GET /' do
    context 'with invalid token' do
      before { get '/api/v1/milestones/', {
        token: 'invalid token',
        roadmap_id: private_admin_roadmap.id
      }}

      it_behaves_like 'a unauthorized request'
    end

    context 'when user admin' do
      before { get '/api/v1/milestones/', {
        token: admin.token,
        roadmap_id: private_admin_roadmap.id
      }}

      it_behaves_like 'a successful request'

      it 'has correct structure' do
        expect(json).to be_a_kind_of(Array)
        expect(json.first).to have_key('id')
        expect(json.first).to have_key('description')
        expect(json.first).to have_key('date_end')
        expect(json.first).to have_key('timeline_id')
        expect(json.first).to have_key('label_id')
        expect(json.first).to have_key('sub_items')
      end

      it 'has correct count' do
        expect(json.count).to eq Milestone.get_list(private_admin_roadmap.id).count
      end
    end

    context 'when user member' do
      context 'with admin, editor rights' do
        before { get '/api/v1/milestones/', {
          roadmap_id: private_member_a_roadmap.id,
          token: member.token
        }}

        it_behaves_like 'a successful request'
      end

      context 'without rights' do
        before { get '/api/v1/milestones/', {
          roadmap_id: private_admin_roadmap.id,
          token: member.token
        }}

        it_behaves_like 'a forbidden request'
      end
    end

    context 'when related roadmap public' do
      before { get '/api/v1/milestones/', { roadmap_id: public_roadmap.id }}

      it_behaves_like 'a successful request'
    end

    context 'when roadmap not found' do
      before { get '/api/v1/milestones/', { roadmap_id: 65656 }}

      it_behaves_like 'not found request'
    end
  end



  describe 'POST /' do
    context 'when invalid data send' do
      before { post '/api/v1/milestones/', {
        token: admin.token,
        description: milestone_data.description,
        timeline_id: nil,
        label_id: Label.first.id,
        date_end: milestone_data.date_end
      }}

      it_behaves_like 'an unprocessable entity request'
    end

    context 'when token invalid' do
      before { post '/api/v1/milestones/', {
        token: 'invalid token',
        description: milestone_data.description,
        timeline_id: Timeline.first.id,
        label_id: Label.first.id,
        date_end: milestone_data.date_end
      }}

      it_behaves_like 'a unauthorized request'
    end

    context 'when user admin' do
      before { post '/api/v1/milestones/', {
        token: admin.token,
        description: milestone_data.description,
        timeline_id: Timeline.first.id,
        label_id: Label.first.id,
        date_end: milestone_data.date_end
      }}

      it_behaves_like 'a created request'

      it 'has correct structure' do
        expect(json).to have_key('id')
      end
    end

    context 'when user member' do
      context 'with admin rights' do
        before { post '/api/v1/milestones/', {
          token: member.token,
          description: milestone_data.description,
          timeline_id: private_member_a_roadmap.timelines.first.id,
          label_id: private_member_a_roadmap.labels.first.id,
          date_end: milestone_data.date_end
        }}

        it_behaves_like 'a created request'
      end

      context 'with editor rights' do
        before { post '/api/v1/milestones/', {
            token: member.token,
            description: milestone_data.description,
            timeline_id: private_member_r_roadmap.timelines.first.id,
            label_id: private_member_r_roadmap.labels.first.id,
            date_end: milestone_data.date_end
        }}

        it_behaves_like 'a forbidden request'
      end

      context 'without rights' do
        before { post '/api/v1/milestones/', {
          token: member.token,
          description: milestone_data.description,
          timeline_id: private_admin_roadmap.timelines.first.id,
          label_id: private_admin_roadmap.labels.first.id,
          date_end: milestone_data.date_end
        }}

        it_behaves_like 'a forbidden request'
      end
    end
  end


  describe 'PUT /:id' do
    context 'when invalid data send' do
      before { put '/api/v1/milestones/' + Milestone.first.id.to_s, {
        token: admin.token,
        description: milestone_data.description,
        label_id: nil,
        date_end: milestone_data.date_end
      }}

      it_behaves_like 'an unprocessable entity request'
    end

    context 'when token invalid' do
      before { put '/api/v1/milestones/' + Milestone.first.id.to_s, {
        token: 'invalid token',
        description: milestone_data.description,
        timeline_id: Timeline.first.id,
        label_id: Label.first.id,
        date_end: milestone_data.date_end
      }}

      it_behaves_like 'a unauthorized request'
    end

    context 'when milestone not found' do
      before { put '/api/v1/milestones/9999', {
        token: admin.token,
        description: milestone_data.description,
        timeline_id: Timeline.first.id,
        label_id: Label.first.id,
        date_end: milestone_data.date_end,
      }}

      it_behaves_like 'not found request'
    end

    context 'when user admin' do
      before { put '/api/v1/milestones/' + Milestone.first.id.to_s, {
        token: admin.token,
        description: milestone_data.description,
        timeline_id: Timeline.first.id,
        label_id: Label.first.id,
        date_end: milestone_data.date_end,
      }}

      it_behaves_like 'a successful request'

      it 'has correct structure' do
        expect(json).to have_key('id')
      end
    end


    context 'when user member' do
      context 'with admin rights' do
        before { put '/api/v1/milestones/' + private_member_a_roadmap.timelines.first.milestones.first.id.to_s, {
          token: member.token,
          description: milestone_data.description,
          timeline_id: Timeline.first.id,
          label_id: private_member_a_roadmap.labels.first.id,
          date_end: milestone_data.date_end,
        }}

        it_behaves_like 'a successful request'
      end

      context 'with reader rights' do
        before { put '/api/v1/milestones/' + private_member_r_roadmap.timelines.first.milestones.first.id.to_s, {
            token: member.token,
            description: milestone_data.description,
            timeline_id: Timeline.first.id,
            label_id: private_member_r_roadmap.labels.first.id,
            date_end: milestone_data.date_end,
        }}

        it_behaves_like 'a forbidden request'
      end

      context 'without rights' do
        before { put '/api/v1/milestones/' + private_admin_roadmap.timelines.first.milestones.first.id.to_s, {
          token: member.token,
          description: milestone_data.description,
          timeline_id: Timeline.first.id,
          label_id: Label.first.id,
          date_end: milestone_data.date_end,
        }}

        it_behaves_like 'a forbidden request'
      end
    end
  end


  describe 'Delete /:id' do
    let(:milestone) { Milestone.first }
    context 'when not found' do
      before { delete '/api/v1/milestones/999999', { token: admin.token }}

      it_behaves_like 'not found request'
    end

    context 'when token invalid' do
      before { delete '/api/v1/milestones/' + milestone.id.to_s, { token: 'invalid token' }}

      it_behaves_like 'a unauthorized request'
    end

    context 'when user admin' do
      before { delete '/api/v1/milestones/' + milestone.id.to_s, { token: admin.token }}

      it_behaves_like 'a successful request'

      it 'has correct response structure' do
        expect(json).to have_key('id')
      end
    end

    #context 'when user member' do
    #  context 'with admin rights' do
    #    before { delete '/api/v1/milestones/' + private_member_a_roadmap.timelines.first.milestones.first.id.to_s, {
    #      token: member.token,
    #    }}
    #
    #    it_behaves_like 'a successful request'
    #  end
    #
    #  context 'with reader rights' do
    #    before { delete '/api/v1/milestones/' + private_member_a_roadmap.timelines.first.milestones.first.id.to_s, {
    #        token: member.token,
    #    }}
    #
    #    it_behaves_like 'a forbidden request'
    #  end
    #
    #  context 'without rights' do
    #    before { delete '/api/v1/milestones/' + private_member_a_roadmap.timelines.first.milestones.first.id.to_s, {
    #      token: member.token,
    #    }}
    #
    #    it_behaves_like 'a forbidden request'
    #  end
    #end
  end
end