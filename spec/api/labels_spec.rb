require 'spec_helper'

describe 'RoadMap::API::Labels ' do
  let(:admin) { User.joins(:roles).where(roles: { name: :Admin }).first }
  let(:member) { User.joins(:roles).where(roles: { name: :Member }).first }
  let(:public_roadmap) { Roadmap.where(is_private: false).first }
  let(:private_member_r_roadmap) {
    Roadmap.joins(:user_roadmaps).where(is_private: true)
    .where(user_roadmaps: { user: member, rights: UserRoadmap::USER_RIGHTS[:reader] }).first
  }
  let(:private_member_a_roadmap) {
    Roadmap.joins(:user_roadmaps).where(is_private: true)
    .where(user_roadmaps: { user: member, rights: UserRoadmap::USER_RIGHTS[:admin] }).first
  }
  let(:private_admin_roadmap) {
    Roadmap.where(is_private: true, owner: admin).first
  }

  describe 'GET /' do

    context 'with invalid token' do
      before { get '/api/v1/labels/', {
        token: 'invalid token',
        roadmap_id: private_admin_roadmap.id
      }}

      it_behaves_like 'a unauthorized request'
    end

    context 'when roadmap not found' do
      before { get '/api/v1/labels/', { roadmap_id: 65656 }}

      it_behaves_like 'not found request'
    end

    context 'when user admin' do
      before { get '/api/v1/labels/', {
        token: admin.token,
        roadmap_id: private_admin_roadmap.id
      }}

      it_behaves_like 'a successful request'

      it 'has correct structure' do
        expect(json).to be_a_kind_of(Array)
        expect(json.first).to have_key('id')
        expect(json.first).to have_key('color_id')
        expect(json.first).to have_key('roadmap_id')
        expect(json.first).to have_key('text')
      end

      it 'has correct count' do
        expect(json.count).to eq Label::LABELS_LIST.count
      end
    end

    context 'when related roadmap public' do
      before { get '/api/v1/labels/', { roadmap_id: public_roadmap.id }}

      it_behaves_like 'a successful request'
    end

    context 'when related roadmap private' do
      context 'when user member' do
        context 'with admin rights' do
          before { get '/api/v1/labels/', {
            roadmap_id: private_member_r_roadmap.id,
            token: member.token
          }}

          it_behaves_like 'a successful request'
        end

        context 'without rights' do
          before { get '/api/v1/labels/', {
            roadmap_id: private_admin_roadmap.id,
            token: member.token
          }}

          it_behaves_like 'a forbidden request'
        end
      end
    end
  end


  describe 'PUT /:id' do
    context 'when token invalid' do
      before { put '/api/v1/labels/' + private_admin_roadmap.labels.first.id.to_s, {
        token: 'invalid token',
        text: 'test text',
        roadmap_id: private_admin_roadmap.id
      }}

      it_behaves_like 'a unauthorized request'
    end

    context 'when label not found' do
      before { put '/api/v1/labels/9999', {
        token: admin.token,
        text: '123',
        roadmap_id: private_admin_roadmap.id
      }}

      it_behaves_like 'not found request'
    end

    context 'when user admin' do
      before { put '/api/v1/labels/' + private_admin_roadmap.labels.first.id.to_s, {
        token: admin.token,
        text: 'test text 2',
        roadmap_id: private_admin_roadmap.id
      }}

      it_behaves_like 'a successful request'

      it 'has correct structure' do
        expect(json).to have_key('id')
      end
    end

    context 'when user member' do
      context 'with admin rights' do
        before { put '/api/v1/labels/' + private_member_a_roadmap.labels.first.id.to_s, {
          token: member.token,
          text: '123',
          roadmap_id: private_member_a_roadmap.id
        }}

        it_behaves_like 'a successful request'
      end

      context 'with reader rights' do
        before { put '/api/v1/labels/' + private_member_r_roadmap.labels.first.id.to_s, {
            token: member.token,
            text: '123',
            roadmap_id: private_member_r_roadmap.id
        }}

        it_behaves_like 'a forbidden request'
      end

      context 'without rights' do
        before { put '/api/v1/labels/' + private_admin_roadmap.labels.first.id.to_s, {
          token: member.token,
          text: 'some title',
          roadmap_id: private_admin_roadmap.id
        }}

        it_behaves_like 'a forbidden request'
      end
    end
  end
end