require 'spec_helper'

describe 'RoadMap::API::UserRoadmaps' do
  let(:admin) { User.joins(:roles).where(roles: { name: :Admin }).first }
  let(:member) { User.joins(:roles).where(roles: { name: :Member }).first }

  let(:public_roadmap) { Roadmap.where(is_private: false).first }
  let(:private_member_r_roadmap) {
    Roadmap.joins(:user_roadmaps).where(is_private: true)
    .where(user_roadmaps: { user: member, rights: UserRoadmap::USER_RIGHTS[:reader] }).first
  }
  let(:private_admin_roadmap) {
    Roadmap.where(is_private: true, owner: admin).first
  }

  let(:admin_ur) { UserRoadmap.where(user: admin).first }
  let(:member_ur) { UserRoadmap.where(user: member, rights: UserRoadmap::USER_RIGHTS[:reader]).first }
  let(:member_ur_admin) { UserRoadmap.where(user: member, rights: UserRoadmap::USER_RIGHTS[:admin]).first }
  let(:new_member) { FactoryGirl.create(:member) }
  let!(:new_member_ur) {
    UserRoadmap.create(roadmap: member_ur_admin.roadmap, user: new_member, rights: UserRoadmap::USER_RIGHTS[:reader])
  }


  describe 'GET /' do
    context 'with invalid token' do
      before { get '/api/v1/user_roadmaps/', { token: 'invalid token', roadmap_id: private_admin_roadmap.id }}

      it_behaves_like 'a unauthorized request'
    end

    context 'when roadmap not found' do
      before { get '/api/v1/user_roadmaps/', { roadmap_id: 65656 }}

      it_behaves_like 'not found request'
    end

    context 'when user admin' do
      before { get '/api/v1/user_roadmaps/', { token: admin.token, roadmap_id: private_admin_roadmap.id }}

      it_behaves_like 'a successful request'

      it 'has correct structure' do
        expect(json).to be_a_kind_of(Array)
        expect(json.first).to have_key('id')
        expect(json.first).to have_key('roadmap_id')
        expect(json.first).to have_key('rights')
        expect(json.first).to have_key('user')
      end

      it 'has correct count' do
        expect(json.count).to eq private_admin_roadmap.user_roadmaps.count
      end
    end

    context 'when related roadmap public' do
      before { get '/api/v1/user_roadmaps/', { roadmap_id: public_roadmap.id }}

      it_behaves_like 'a successful request'
    end

    context 'when related roadmap private' do
      context 'when user member' do
        context 'with admin, reader rights' do
          before { get '/api/v1/user_roadmaps/', { roadmap_id: private_member_r_roadmap.id, token: member.token }}

          it_behaves_like 'a successful request'
        end

        context 'without rights' do
          before { get '/api/v1/user_roadmaps/', {
            roadmap_id: private_admin_roadmap.id,
            token: member.token
          }}

          it_behaves_like 'a forbidden request'
        end
      end
    end
  end


  describe 'POST /' do
    context 'when invalid data send' do
      before { post '/api/v1/user_roadmaps/', {
        token: admin.token,
        roadmap_id: Roadmap.first.id,
        user_id: '',
        rights: nil
      }}

      it_behaves_like 'an unprocessable entity request'
    end

    context 'when token invalid' do
      before { post '/api/v1/user_roadmaps', {
        token: 'invalid token',
        roadmap_id: Roadmap.first.id,
        user_id: member.id,
        rights: UserRoadmap::USER_RIGHTS[:admin]
      }}

      it_behaves_like 'a unauthorized request'
    end

    context 'when user admin' do
      before { post '/api/v1/user_roadmaps/', {
        token: admin.token,
        roadmap_id: Roadmap.first.id,
        user_id: new_member.id,
        rights: UserRoadmap::USER_RIGHTS[:admin]
      }}

      it_behaves_like 'a created request'

      it 'has correct structure' do
        expect(json).to have_key('id')
      end
    end

    context 'when user is member' do
      context 'with reader rights' do
        describe 'add member with admin rights' do
          before {
            roadmap = Roadmap.joins(:user_roadmaps)
                                .where(user_roadmaps: { user: member, rights: UserRoadmap::USER_RIGHTS[:reader] }).first
            post '/api/v1/user_roadmaps/', {
              token: member.token,
              roadmap_id: roadmap.id,
              user_id: member.id,
              rights: UserRoadmap::USER_RIGHTS[:admin]
          }}

          it_behaves_like 'a forbidden request'
        end
      end
    end

    context 'when user without rights' do
      before {
        roadmap = Roadmap.joins(:user_roadmaps).where(user_roadmaps: { user: admin }).first
        post '/api/v1/user_roadmaps/', {
          token: member.token,
          roadmap_id: roadmap.id,
          user_id: member.id,
          rights: UserRoadmap::USER_RIGHTS[:admin]
      }}

      it_behaves_like 'a forbidden request'
    end
  end


  describe 'PUT /:id' do
    context 'when not found' do
      before { put '/api/v1/user_roadmaps/9999', {
        token: admin.token,
        roadmap_id: admin_ur.roadmap.id,
        user_id: member.id,
        rights: UserRoadmap::USER_RIGHTS[:admin]
      }}

      it_behaves_like 'not found request'
    end

    describe 'change rights of owner' do
      before {
        roadmap = Roadmap.where(owner: member).first
        user_roadmap = UserRoadmap.where(roadmap: roadmap, user: member).first
        put '/api/v1/user_roadmaps/' + user_roadmap.id.to_s, {
          token: admin.token,
          rights: UserRoadmap::USER_RIGHTS[:reader]
      }}

      it_behaves_like 'a forbidden request'
    end

    context 'when invalid data send' do
      before { put '/api/v1/user_roadmaps/' + new_member_ur.id.to_s, {
        token: admin.token,
        rights: nil
      }}

      it_behaves_like 'an unprocessable entity request'
    end

    context 'when token valid' do
      before { put '/api/v1/user_roadmaps/' + new_member_ur.id.to_s, {
        token: admin.token,
        rights: UserRoadmap::USER_RIGHTS[:admin]
      }}

      it_behaves_like 'a successful request'

      it 'has correct structure' do
        expect(json).to have_key('id')
      end
    end

    context 'when token invalid' do
      before { put '/api/v1/user_roadmaps' + admin_ur.id.to_s, {
        token: 'invalid token',
        rights: UserRoadmap::USER_RIGHTS[:admin]
      }}

      it_behaves_like 'a unauthorized request'
    end

    context 'when user is member' do
      context 'with reader rights' do
        describe 'add member with admin rights' do
          before {
            put '/api/v1/user_roadmaps/' + member_ur.id.to_s, {
              token: member.token,
              rights: UserRoadmap::USER_RIGHTS[:admin]
            }}

          it_behaves_like 'a forbidden request'
        end
      end
    end

    context 'when user without rights' do
      before {
        put '/api/v1/user_roadmaps/' + admin_ur.id.to_s, {
          token: member.token,
          rights: UserRoadmap::USER_RIGHTS[:admin]
        }}

      it_behaves_like 'a forbidden request'
    end
  end


  describe 'DELETE /:id' do
    context 'when user is admin' do
      context 'when user_roadmap not found' do
        before { delete '/api/v1/user_roadmaps/999', { token: admin.token }}

        it_behaves_like 'not found request'
      end

      describe 'delete owner' do
        before {
          roadmap = Roadmap.where(owner: member).first
          user_roadmap = UserRoadmap.where(roadmap: roadmap, user: member).first
          delete '/api/v1/user_roadmaps/' + user_roadmap.id.to_s, {
            token: admin.token,
        }}

        it_behaves_like 'a forbidden request'
      end

      context 'when token valid' do
        before { delete '/api/v1/user_roadmaps/' + new_member_ur.id.to_s, { token: admin.token }}

        it_behaves_like 'a successful request'

        it 'has correct response structure' do
          expect(json).to have_key('id')
        end
      end

      context 'when token invalid' do
        before { delete '/api/v1/roadmaps/' + new_member_ur.id.to_s, { token: 'invalid token' }}

        it_behaves_like 'a unauthorized request'
      end

      context 'when user is member' do
        context 'with reader rights' do
          describe 'delete member with admin rights' do
            before { delete '/api/v1/user_roadmaps/' + member_ur_admin.id.to_s, { token: new_member.token}}

            it_behaves_like 'a forbidden request'
          end
        end
      end

      context 'when user without rights' do
        before { delete '/api/v1/user_roadmaps/' + admin_ur.id.to_s, { token: member.token }}

        it_behaves_like 'a forbidden request'
      end
    end
  end
end