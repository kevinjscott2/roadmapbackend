require 'spec_helper'

describe 'RoadMap::API::SubItem' do
  let(:admin) { User.joins(:roles).where(roles: { name: :Admin }).first }
  let(:member) { User.joins(:roles).where(roles: { name: :Member }).first }
  let(:member_a_sub_item) {
    Roadmap.joins(:user_roadmaps)
            .where(is_private: true, user_roadmaps: { user: member, rights: UserRoadmap::USER_RIGHTS[:admin] }).first
            .timelines.first.milestones.first.sub_items.first
  }
  let(:member_r_sub_item) {
    Roadmap.joins(:user_roadmaps)
            .where(is_private: true, user_roadmaps: { user: member, rights: UserRoadmap::USER_RIGHTS[:reader] }).first
            .timelines.first.milestones.first.sub_items.first
  }
  let(:admin_sub_item) {
    Roadmap.where(is_private: true, owner: admin).first.timelines.first.milestones.first.sub_items.first
  }

  describe 'POST /' do
    context 'when invalid data send' do
      before { post '/api/v1/sub_items/', {
        token: admin.token,
        name: '',
        milestone_id: admin_sub_item.milestone_id
      }}

      it_behaves_like 'an unprocessable entity request'
    end

    context 'when token invalid' do
      before { post '/api/v1/sub_items', {
        token: 'invalid token',
        name: admin_sub_item.name,
        milestone_id: admin_sub_item.milestone_id
      }}

      it_behaves_like 'a unauthorized request'
    end

    context 'when user admin' do
      before { post '/api/v1/sub_items/', {
        token: admin.token,
        name: admin_sub_item.name,
        milestone_id: admin_sub_item.milestone_id
      }}

      it_behaves_like 'a created request'

      it 'has correct structure' do
        expect(json).to have_key('id')
      end
    end

    context 'when user member' do
      context 'with admin rights' do
        before { post '/api/v1/sub_items/', {
          token: member.token,
          name: member_a_sub_item.name,
          milestone_id: member_a_sub_item.milestone_id
        }}

        it_behaves_like 'a created request'
      end

      context 'with reader rights' do
        before { post '/api/v1/sub_items/', {
            token: member.token,
            name: member_r_sub_item.name,
            milestone_id: member_r_sub_item.milestone_id
        }}

        it_behaves_like 'a forbidden request'
      end

      context 'without rights' do
        before { post '/api/v1/sub_items/', {
          token: member.token,
          name: admin_sub_item.name,
          milestone_id: admin_sub_item.milestone_id
        }}

        it_behaves_like 'a forbidden request'
      end
    end
  end

  describe 'PUT /sort/:milestone_id' do
    context 'when not found' do
      before { put '/api/v1/sub_items/sort/99999', {
        token: admin.token,
        sorted_ids: admin_sub_item.milestone.sub_items.pluck(:id).join(',')
      }}

      it_behaves_like 'not found request'
    end

    context 'when token invalid' do
      before { put '/api/v1/sub_items/sort/' + admin_sub_item.milestone.id.to_s, { token: 'invalid token' }}

      it_behaves_like 'a unauthorized request'
    end

    context 'when user admin' do
      before { put '/api/v1/sub_items/sort/' + admin_sub_item.milestone.id.to_s, {
        token: admin.token,
        sorted_ids: admin_sub_item.milestone.sub_items.pluck(:id).join(',')
      }}

      it_behaves_like 'a successful request'
    end

    context 'when user member' do
      context 'with admin rights' do
        before { put '/api/v1/sub_items/sort/' + member_a_sub_item.milestone.id.to_s, {
          token: member.token,
          sorted_ids: member_a_sub_item.milestone.sub_items.pluck(:id).join(',')
        }}

        it_behaves_like 'a successful request'
      end

      context 'with reader rights' do
        before { put '/api/v1/sub_items/sort/' + member_r_sub_item.milestone.id.to_s, {
            token: member.token,
            sorted_ids: member_r_sub_item.milestone.sub_items.pluck(:id).join(',')
        }}

        it_behaves_like 'a forbidden request'
      end

      context 'without rights' do
        before { put '/api/v1/sub_items/sort/' + admin_sub_item.milestone.id.to_s, {
          token: member.token,
          sorted_ids: admin_sub_item.milestone.sub_items.pluck(:id).join(',')
        }}

        it_behaves_like 'a forbidden request'
      end
    end
  end

  describe 'PUT /:id' do
    context 'when not found' do
      before { put '/api/v1/sub_items/99999', {
          token: admin.token,
          name: 'New name'
      }}

      it_behaves_like 'not found request'
    end

    context 'when token invalid' do
      before { put '/api/v1/sub_items/' + admin_sub_item.id.to_s, { token: 'invalid token' }}

      it_behaves_like 'a unauthorized request'
    end

    context 'when name is invalid' do
      before { put '/api/v1/sub_items/' + admin_sub_item.id.to_s, { token: admin.token, name: '' }}

      it_behaves_like 'an unprocessable entity request'
    end

    context 'when user admin' do
      let(:sub_item) { admin_sub_item }
      before { put '/api/v1/sub_items/' + sub_item.id.to_s, {
          token: admin.token,
          name: 'New name'
      }}

      it_behaves_like 'a successful request'

      it 'renames the sub item to New name' do
        expect(SubItem.find(sub_item.id).name).to eq 'New name'
      end
    end

    context 'when user member' do
      context 'with admin rights' do
        before { put '/api/v1/sub_items/' + member_a_sub_item.id.to_s, {
            token: member.token,
            name: 'New name'
        }}

        it_behaves_like 'a successful request'
      end

      context 'with reader rights' do
        before { put '/api/v1/sub_items/' + member_r_sub_item.id.to_s, {
            token: member.token,
            name: 'New name'
        }}

        it_behaves_like 'a forbidden request'
      end

      context 'without rights' do
        before { put '/api/v1/sub_items/' + admin_sub_item.id.to_s, {
            token: member.token,
            name: 'New name'
        }}

        it_behaves_like 'a forbidden request'
      end
    end
  end


  describe 'Delete /:id' do
    context 'when not found' do
      before { delete '/api/v1/sub_items/99999', { token: admin.token }}

      it_behaves_like 'not found request'
    end

    context 'when token invalid' do
      before { delete '/api/v1/sub_items/' + admin_sub_item.id.to_s, { token: 'invalid token' }}

      it_behaves_like 'a unauthorized request'
    end

    context 'when user admin' do
      let(:sub_item) { admin_sub_item }
      before { delete '/api/v1/sub_items/' + sub_item.id.to_s, { token: admin.token }}

      it_behaves_like 'a successful request'

      it 'has correct response structure' do
        expect(json).to have_key('id')
      end
    end

    context 'when user member' do
      context 'with admin, reader rights' do
        before { delete '/api/v1/sub_items/' + member_a_sub_item.id.to_s, { token: member.token }}

        it_behaves_like 'a successful request'
      end

      context 'with admin, reader rights' do
        before { delete '/api/v1/sub_items/' + member_r_sub_item.id.to_s, { token: member.token }}

        it_behaves_like 'a forbidden request'
      end

      context 'without rights' do
        before { delete '/api/v1/sub_items/' + admin_sub_item.id.to_s, { token: member.token }}

        it_behaves_like 'a forbidden request'
      end
    end
  end
end