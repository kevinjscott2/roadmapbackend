require 'spec_helper'

describe 'RoadMap::API::Users' do
  let(:admin) { User.joins(:roles).where(roles: { name: :Admin }).first }
  let(:member) { User.joins(:roles).where(roles: { name: :Member }).first }


  describe 'POST /auth' do
    let(:valid_access_token) { 'valid token' }
    let(:invalid_access_token) { 'invalid token' }

    {google: OauthHelper::Google, facebook: OauthHelper::Facebook, github: OauthHelper::Github}.each do |provider, oauth_helper|
      context "when valid #{provider} api token" do
        let(:user_email) { member.email }
        before(:each) do
          expect(oauth_helper).to receive(:get_user_email).with(valid_access_token).and_return(user_email)
          post '/api/v1/users/auth', { access_token: valid_access_token, provider: provider }
        end

        context 'when user is member' do

          it_behaves_like 'a created request'

          it 'respond with info of signed in member' do
            member.reload
            expect(json).to have_key('id')
            expect(json).to have_key('token')
            expect(json).to have_key('second_email')
            expect(json).to have_key('avatar')
            expect(json).to have_key('avatar_thumb')
            expect(json).to have_key('email')
            expect(json).to have_key('role')
            expect(json['id']).to eq member.id
            expect(json['token']).to eq member.token
            expect(json['email']).to eq member.email
            expect(json['second_email']).to eq member.second_email
            expect(json['role']).to eq member.roles.first.id
          end
        end

        context 'when user not register' do
          let(:user_email) { 'not_member@gmail.com' }

          it_behaves_like 'a created request'
        end
      end

      context "when invalid #{provider} api token" do
        before(:each) do
          expect(oauth_helper).to receive(:get_user_email).with(invalid_access_token).and_return(nil)
          post '/api/v1/users/auth', { access_token: invalid_access_token, provider: provider }
        end

        it_behaves_like 'not found request'
      end
    end

    context 'when invalid OAuth provider specified' do
      before do
        post '/api/v1/users/auth', { access_token: valid_access_token, provider: 'shmeisebook' }
      end

      it_behaves_like 'a bad request'
    end

    let(:parameters) { { access_token: valid_access_token, provider: 'google' } }

    [:access_token, :provider].each do |parameter|
      context "when OAuth #{parameter} parameter is not specified" do

        before do
          post '/api/v1/users/auth', parameters.dup.except!(parameter)
        end

        it_behaves_like 'a bad request'
      end
    end
  end


  describe 'GET /' do
    context 'when user is member' do
      before { get '/api/v1/users', { token: member.token} }

      it_behaves_like 'a forbidden request'
    end

    context 'when invalid token' do
      before { get '/api/v1/users', { token: 'invalid token'} }

      it_behaves_like 'a unauthorized request'
    end

    context 'when user admin' do
      before { get '/api/v1/users', { token: admin.token} }

      it_behaves_like 'a successful request'

      it 'has correct structure' do
        expect(json).to be_a_kind_of(Array)
        expect(json.first).to have_key('id')
        expect(json.first).to have_key('email')
        expect(json.first).to have_key('second_email')
      end

      it 'has correct count' do
        expect(json.count).to eq User.all.count
      end
    end
  end


  describe 'GET /search' do
    context 'when invalid token' do
      before { get '/api/v1/users/search', { token: 'invalid token'} }

      it_behaves_like 'a unauthorized request'
    end

    context 'when valid token' do
      context 'with set member_only' do
        before { get '/api/v1/users/search', { token: admin.token, member_only: true, email: 'admi' }}

        it_behaves_like 'a successful request'

        it 'has correct count' do
          expect(json.count).to eq User.joins(:roles).where(roles: { name: :Member }).count
        end
      end

      context 'with not set member_only' do
        before { get '/api/v1/users/search', { token: admin.token, email: 'admi'}}

        it_behaves_like 'a successful request'

        it 'has correct count' do
          expect(json.count).to eq User.all.count
        end
      end
    end
  end


  describe 'POST /' do
    let!(:new_user) { FactoryGirl.build(:user) }

    context 'when invalid data send' do
      before { post '/api/v1/users/', { token: member.token, email: '' }}

      it_behaves_like 'an unprocessable entity request'
    end

    context 'when token valid' do
      before { post '/api/v1/users/', { token: member.token, email: new_user.email }}

      it_behaves_like 'a created request'

      it 'has correct structure' do
        expect(json).to have_key('id')
      end
    end

    context 'when token invalid' do
      before { post '/api/v1/users', { token: 'invalid token', email: new_user.email }}

      it_behaves_like 'a unauthorized request'
    end
  end

  describe 'PUT /:id' do
    context 'when token valid' do
      before { put '/api/v1/users/' + member.id.to_s, {
        token: admin.token,
        email: "#{member.email}m",
        role_id: Role.first.id
      }}

      it_behaves_like 'a successful request'

      it 'has correct structure' do
        expect(json).to have_key('id')
      end
    end

    context 'when token invalid' do
      before { put '/api/v1/users/' + member.id.to_s, {
        token: 'invalid token',
        email: "#{member.email}m",
        role_id: Role.first.id
      }}

      it_behaves_like 'a unauthorized request'
    end

    context 'when user update himself' do
      before { put '/api/v1/users/' + member.id.to_s, {
          token: member.token,
          email: "#{member.email}m",
          second_email: "#{member.email}dm",
          role_id: Role.first.id
      }}

      it_behaves_like 'a successful request'
    end

    context 'when user member' do
      before { put '/api/v1/users/' + admin.id.to_s, {
        token: member.token,
        email: "#{member.email}m",
        role_id: Role.first.id
      }}

      it_behaves_like 'a forbidden request'
    end

    context 'when data invalid' do
      before { put '/api/v1/users/' + member.id.to_s, {
        token: admin.token,
        email: 'invalid email',
        role_id: Role.first.id
      }}

      it_behaves_like 'an unprocessable entity request'
    end
  end


  describe 'POST /upload_avatar' do
    context 'when token valid' do
      before { post '/api/v1/users/upload_avatar/', {
          token: admin.token,
          avatar: fixture_file_upload(Rails.root.join('spec', 'attachments', 'avatar.png'), 'image/png')
      }}

      it_behaves_like 'a created request'

      it 'has correct structure' do
        admin.reload
        expect(json).to have_key('id')
        expect(json).to have_key('token')
        expect(json).to have_key('second_email')
        expect(json).to have_key('avatar')
        expect(json).to have_key('avatar_thumb')
        expect(json).to have_key('email')
        expect(json).to have_key('role')
        expect(json['id']).to eq admin.id
        expect(json['token']).to eq admin.token
        expect(json['email']).to eq admin.email
        expect(json['second_email']).to eq admin.second_email
        expect(json['role']).to eq admin.roles.first.id
      end
    end

    context 'when token invalid' do
      before { post '/api/v1/users/upload_avatar/', {
          token: 'invalid token',
          avatar: fixture_file_upload(Rails.root.join('spec', 'attachments', 'avatar.png'), 'image/png')
      }}

      it_behaves_like 'a unauthorized request'
    end



    context 'when data invalid' do
      before { post '/api/v1/users/upload_avatar/', {
          token: admin.token,
          avatar: fixture_file_upload(Rails.root.join('spec', 'attachments', 'avatar.txt'), 'plain\text')
      }}

      it_behaves_like 'an unprocessable entity request'
    end
  end

  describe 'Delete /:id' do
    context 'when user not found' do
      before { delete '/api/v1/users/555', {
          token: admin.token
      }}

      it_behaves_like 'not found request'
    end

    context 'when token valid' do
      before {
        FactoryGirl.create(:user)
        delete '/api/v1/users/' + User.last.id.to_s, { token: admin.token }
      }

      it_behaves_like 'a successful request'

      it 'has correct response structure' do
        expect(json).to have_key('id')
      end
    end

    context 'when token invalid' do
      before { delete '/api/v1/users/' + User.last.id.to_s, {
          token: 'invalid token'
      }}

      it_behaves_like 'a unauthorized request'
    end

    context 'when member delete his account' do
      before {
        delete '/api/v1/users/' + member.id.to_s, { token: member.token }
      }

      it_behaves_like 'a successful request'
    end

    context 'when member delete not his account' do
      before {
        delete '/api/v1/users/' + admin.id.to_s, { token: member.token }
      }

      it_behaves_like 'a forbidden request'
    end
  end
end