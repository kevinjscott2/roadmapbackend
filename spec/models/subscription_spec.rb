require 'spec_helper'

describe Subscription do
  specify { should belong_to(:user) }
  specify { should belong_to(:roadmap) }
  specify { should validate_uniqueness_of(:user_id).scoped_to(:roadmap_id) }
end
