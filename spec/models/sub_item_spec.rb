require 'spec_helper'

describe SubItem do
  let!(:admin) { User.joins(:roles).where(roles: { name: :Admin }).first }

  specify { should belong_to(:milestone) }
  specify { should validate_presence_of :name }
  specify { should validate_presence_of :milestone }
  specify { should validate_numericality_of(:position).only_integer }

  let!(:sub_item) { SubItem.create(
      name: '1123',
      milestone: Milestone.first,
      activity_author: admin
  )}

  it_behaves_like 'model activity', :create do
    let(:entity) { sub_item }
    let(:activity_author) { admin }
  end

  it_behaves_like 'model activity', :update do
    let!(:entity) {
      sub_item.update(name: '123')
      sub_item
    }
    let(:activity_author) { admin }
  end

  it_behaves_like 'model activity', :destroy do
    let!(:entity) { sub_item.destroy }
    let(:activity_author) { admin }
  end
end