require 'spec_helper'

describe Label do
  specify { should belong_to(:roadmap) }
  specify { should validate_presence_of :roadmap }
  specify { should validate_presence_of :color_id }
  specify { should validate_numericality_of(:color_id).only_integer }
end