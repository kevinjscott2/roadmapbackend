require 'spec_helper'

describe Milestone do
  let!(:admin) { User.joins(:roles).where(roles: { name: :Admin }).first }

  specify { should have_many(:sub_items).dependent(:destroy) }
  specify { should belong_to(:timeline) }
  specify { should belong_to(:label) }
  specify { should validate_presence_of :description }
  specify { should validate_presence_of :timeline }
  specify { should validate_presence_of :label }

  describe '#date_end' do
    context 'when invalid date' do
      it 'not have error' do
        expect(Milestone.create).to have(1).error_on(:date_end)
      end
    end
  end

  describe '#label' do
    let(:milestone) { Milestone.first }
    context 'when not current roadmap labels' do
      it 'not have error' do
        milestone.update(label: Label.last)
        expect(milestone).to have(1).error_on(:label)
      end
    end
  end


  let!(:milestone) { Milestone.create(
      description: '1',
      timeline_id: Timeline.first.id,
      label_id: Label.first.id,
      date_end: Date.today,
      activity_author: admin
  )}

  it_behaves_like 'model activity', :create do
    let(:entity) { milestone }
    let(:activity_author) { admin }
  end

  it_behaves_like 'model activity', :update do
    let!(:entity) {
      milestone.update(description: '123')
      milestone
    }
    let(:activity_author) { admin }
  end

  it_behaves_like 'model activity', :destroy do
    let!(:entity) { milestone.destroy }
    let(:activity_author) { admin }
  end
end