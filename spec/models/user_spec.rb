require 'spec_helper'

describe User do
  let(:admin) { User.joins(:roles).where(roles: { name: :Admin }).first }
  specify { should validate_presence_of :email }
  specify { should validate_uniqueness_of :email }
  specify { should have_many(:user_roadmaps).dependent(:destroy) }
  specify { should have_many(:roadmaps).through(:user_roadmaps) }
  specify { should have_attached_file(:avatar) }
  specify { should validate_attachment_size(:avatar).less_than(2.megabytes) }
  specify { should validate_attachment_content_type(:avatar).allowing(User::DOC_TYPES).rejecting('text/plain', 'text/xml') }

  describe '#token' do
    context 'when is blank' do

      before(:each) do
        @user1 = FactoryGirl.create(:user_without_token)
        @user2 = FactoryGirl.create(:user_without_token)
      end
      it 'not require case sensitive unique value' do
        expect(@user2).to be_valid
      end
    end

    context 'when not blank' do
      specify { should  validate_uniqueness_of :token }
    end
  end

  describe '.auth' do
    let(:user) { FactoryGirl.create(:user) }
    context 'when user auth with email in database' do

      it 'return user object' do
        expect(User.auth(user.email)).to eq user
      end
    end

    context 'when user auth with email not in database' do
      it 'add user and return his object' do
        expect(User.auth('timkov2@mlsdev.com')).to eq User.find_by(email: 'timkov2@mlsdev.com')
      end
    end
  end

  describe '.safe_destroy' do
    before do
      @user_1 = FactoryGirl.create(:user)
      @roadmap_1 = FactoryGirl.create(:roadmap, user: @user_1)
      @roadmap_2 = FactoryGirl.create(:roadmap, user: @user_1)
      @user_roadmap = FactoryGirl.create(:user_roadmap, user: admin, roadmap: @roadmap_2, rights: UserRoadmap::USER_RIGHTS[:reader])
      @user_1.safe_destroy
    end

    it 'delete roadmap without other users connect' do
      expect(Roadmap.exists?(@roadmap_1)).to eq nil
    end
    it 'update other users rights to :admin' do
      expect { @user_roadmap.reload }.to change(@user_roadmap, :rights).from(UserRoadmap::USER_RIGHTS[:reader]).to(UserRoadmap::USER_RIGHTS[:admin])
    end
    it 'delete user' do
      expect(User.exists?(@user_1)).to eq nil
    end
  end
end