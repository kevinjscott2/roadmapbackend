require 'spec_helper'

describe Roadmap do
  let!(:admin) { User.joins(:roles).where(roles: { name: :Admin }).first }
  let!(:roadmap) { Roadmap.create(name: 'asd asfsg', owner: admin, is_archive: true, is_private: true) }

  specify { should validate_presence_of :name }
  specify { should have_many(:timelines).dependent(:destroy) }
  specify { should have_many(:labels).dependent(:destroy) }
  specify { should validate_presence_of :owner }
  specify { should belong_to(:owner).class_name('User').with_foreign_key('owner_id') }
  specify { should have_many(:user_roadmaps).dependent(:destroy) }
  specify { should have_many(:users).through(:user_roadmaps) }

  context 'when new record create' do
    let(:new_roadmap) { FactoryGirl.create(:roadmap, user: FactoryGirl.create(:member)) }

    it 'create labels list' do
      expect(new_roadmap.labels.count).to eq Label::LABELS_LIST.count
    end
  end


  it_behaves_like 'model activity', :update do
    let!(:entity) {
      roadmap.activity_author = admin
      roadmap.update(name: 'hello')
      roadmap
    }
    let(:activity_author) { admin }
  end
end