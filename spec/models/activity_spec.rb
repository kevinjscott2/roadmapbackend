require 'spec_helper'

describe Activity do
  specify { should belong_to(:roadmap) }
  specify { should belong_to(:user) }
end