require 'spec_helper'

describe UserRoadmap do
  let!(:admin) { User.joins(:roles).where(roles: { name: :Admin }).first }

  specify { should validate_presence_of :rights }
  specify { should validate_numericality_of(:rights).only_integer }
  specify { should belong_to(:user) }
  specify { should belong_to(:roadmap) }

  let!(:user_roadmap) { UserRoadmap.create(
      roadmap: Roadmap.first,
      user: User.last,
      rights: UserRoadmap::USER_RIGHTS[:admin],
      activity_author: admin
  )}

  it_behaves_like 'model activity', :create do
    let(:entity) { user_roadmap }
    let(:activity_author) { admin }
  end

  it_behaves_like 'model activity', :update do
    let!(:entity) {
      user_roadmap.update(rights: UserRoadmap::USER_RIGHTS[:reader])
      user_roadmap
    }
    let(:activity_author) { admin }
  end


  it_behaves_like 'model activity', :destroy do
    let!(:entity) { user_roadmap.destroy }
    let(:activity_author) { admin }
  end
end