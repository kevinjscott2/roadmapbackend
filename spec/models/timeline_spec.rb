require 'spec_helper'

describe Timeline do
  let(:roadmap) { roadmap = Roadmap.last }
  let!(:admin) { User.joins(:roles).where(roles: { name: :Admin }).first }


  specify { should validate_presence_of :title }
  specify { should belong_to(:roadmap) }
  specify { should have_many(:milestones).dependent(:destroy) }

  let!(:timeline) { Timeline.create(
      title: '1123',
      roadmap: roadmap,
      activity_author: admin
  )}


  it_behaves_like 'model activity', :create do
    let(:entity) { timeline }
    let(:activity_author) { admin }
  end



  it_behaves_like 'model activity', :update do
    let!(:entity) {
      timeline.update(title: '123')
      timeline
    }
    let(:activity_author) { admin }
  end

  describe do
    before {
      @timeline_copy = timeline.copy_to(roadmap)
      @timeline_copy.activity_author = admin
      @timeline_copy.save
    }
    it_behaves_like 'model activity', :copy do
      let(:entity) {
        @timeline_copy
      }
      let(:activity_author) { admin }
    end
  end

  it_behaves_like 'model activity', :destroy do
    let!(:entity) { timeline.destroy }
    let(:activity_author) { admin }
  end



  describe '#copy_to' do
    let(:timeline) { Timeline.includes(:milestones).where.not(milestones: {timeline_id: nil}).references(:milestones).take }
    let(:roadmap) { Roadmap.joins(:timelines).where("timelines.roadmap_id != #{timeline.id}").first }
    let(:timeline_copy) { timeline.copy_to(roadmap) }

    context 'returns a new timeline' do
      it do
        expect(timeline_copy.new_record?).to eq true
      end

      it 'assigned to new roadmap' do
        expect(timeline_copy.roadmap_id).to eq roadmap.id
      end

      it 'with copied title' do
        expect(timeline_copy.title).to eq "#{timeline.title} copy"
      end

      it 'with copied milestones' do
        milestones_copy_data = timeline_copy.milestones.map{|m| [m.date_end, m.description]}
        milestones_orig_data = timeline.milestones.map{|m| [m.date_end, m.description]}

        expect(milestones_copy_data).to eq milestones_orig_data
        expect(timeline_copy.milestones.map(&:new_record?).all?).to eq true
      end

      it 'with milestones with copied subitems' do
        subitems_copy_data = timeline_copy.milestones.map(&:sub_items).map{|sub_items| sub_items.map{|s| [s.name, s.position]}}
        subitems_orig_data = timeline.milestones.map(&:sub_items).map{|sub_items| sub_items.map{|s| [s.name, s.position]}}
        subitems_new_record = timeline_copy.milestones.map(&:sub_items).map{|sub_items| sub_items.map(&:new_record?) }.flatten.all?

        expect(subitems_copy_data).to eq subitems_orig_data
        expect(subitems_new_record).to eq true
      end

      it 'with milestones and labels from new roadmap' do
        expect(timeline_copy.milestones.map(&:label_id).uniq - roadmap.labels.pluck(:id)).to be_empty
      end

      it 'with milestones and labels from new roadmap with the same color' do
        expect(timeline_copy.milestones.map(&:label).map(&:color_id)).to eq timeline.milestones.map(&:label).map(&:color_id)
      end
    end
  end

  describe '#move_to' do
    let(:timeline) { Timeline.includes(:milestones).where.not(milestones: {timeline_id: nil}).references(:milestones).take }
    let(:roadmap) { Roadmap.joins(:timelines).where.not(timelines: {roadmap_id: timeline.id}).first }

    it 'saves moved timeline' do
      expect(timeline.move_to(roadmap)).to eq true
    end

    it 'changes roadmap' do
      expect { timeline.move_to(roadmap) }.to change{ Timeline.find(timeline.id).roadmap_id }.from(timeline.roadmap_id).to(roadmap.id)
    end

    it 'changes labels of milestones regarding to the new roadmap' do
      timeline_orig = Timeline.includes(:milestones).find(timeline.id)
      timeline.move_to(roadmap)
      moved_timeline = Timeline.find(timeline.id)
      expect(moved_timeline.milestones.map(&:label_id).uniq - roadmap.labels.pluck(:id)).to be_empty
      expect(moved_timeline.milestones.map(&:label).map(&:color_id)).to eq timeline_orig.milestones.map(&:label).map(&:color_id)
    end
  end
end