class Subscription < ActiveRecord::Base
  belongs_to :user
  belongs_to :roadmap

  validates_uniqueness_of :user_id, scope: :roadmap_id

  scope :active, -> { where(is_subscribed: true) }
end
