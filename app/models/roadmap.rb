class Roadmap < ActiveRecord::Base
  include Concerns::HashedModel
  include Concerns::ModelActivity

  attr_accessor :import

  has_many :activities, dependent: :destroy
  has_many :timelines, dependent: :destroy, inverse_of: :roadmap
  has_many :user_roadmaps, dependent: :destroy
  has_many :labels, dependent: :destroy, inverse_of: :roadmap
  has_many :users, through: :user_roadmaps
  has_many :subscriptions
  belongs_to :owner, class_name: 'User', foreign_key: 'owner_id'

  validates :name, presence: true
  validates :is_archive, inclusion: { in: [true, false] }
  validates :is_private, inclusion: { in: [true, false] }
  validates :owner, presence: true
  validate :labels_presence, if: ->() { self.import }

  scope :active, -> { where(is_archive: false) }
  scope :archive, -> { where(is_archive: true) }
  scope :public, -> { where(is_private: false) }
  scope :private, -> { where(is_private: true) }
  scope :available, -> (user_id) { joins(:user_roadmaps).where(user_roadmaps: { user_id: user_id }) }

  after_initialize :default_values
  after_create :init_labels, unless: ->() { self.import }
  #after_create :grant_admin_rights_for_owner

  accepts_nested_attributes_for :timelines
  accepts_nested_attributes_for :labels

  def self.get_available(user)
    if user.has_role?(:Admin)
      all
    elsif user.has_role?(:Member)
      includes(:user_roadmaps).where(user_roadmaps: { user_id: user.id })
    end
  end

  def prepare_changes
    super unless new_record?
  end

  def get_activity_roadmap
    id
  end

  def present_for_activity
    presentation = super
    presentation.update(name: self.name, hashed_id: self.hashed_id)
  end

  def seed_test_data
    t = self.timelines.build(title: 'This is sample timeline')
    milestone_data = {
        date_end: 1.week.ago,
        description: 'My milestone',
        label_id: self.labels.first.id
    }
    t.milestones.build(milestone_data)
    m = t.milestones.build(milestone_data.merge(description: 'A different milestone'))
    m.sub_items.build(name: 'Milestones can have lists')
    m.sub_items.build(name: 'Bulleted items can be reordered')
    m.sub_items.build(name: 'Add as many bullets as you like...or none at all')
    save
  end

  private

  def default_values
    if self.new_record?
      self.is_archive ||= false
      self.is_private ||= true
    end
  end

  def init_labels
    Label::LABELS_LIST.each_index do |index|
      Label.create(color_id: index, roadmap: self)
    end
  end

  # Used only for import
  def labels_presence
    if self.labels.length != Label::LABELS_LIST.count
      errors.add(:labels, "There are should be #{Label::LABELS_LIST.count} labels present")
    elsif self.labels.map { |label| label.color_id }.sort != Label::LABELS_LIST.each_with_index.map { |e, index| index}
      errors.add(:labels, "There are should be labels with unique color id from 0 to #{Label::LABELS_LIST.count - 1}")
    end
  end

  def grant_admin_rights_for_owner
    user_roadmaps.create(
      user_id: self.owner_id,
      rights: UserRoadmap::USER_RIGHTS[:admin]
    )
  end
end