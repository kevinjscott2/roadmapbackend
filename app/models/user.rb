class User < ActiveRecord::Base
  rolify

  DOC_TYPES = [%w(image/jpg image/jpeg image/png image/gif)]
  DOC_EXT = %w(jpg jpeg png gif)

  has_many :user_roadmaps, dependent: :destroy
  has_many :roadmaps, through: :user_roadmaps
  has_many :subscriptions

  has_attached_file :avatar,
                    hash_secret: 'roadmapbackend',
                    url: "/assets/:rails_env/images/avatars/:hash.:extension",
                    path: ":rails_root/public/assets/:rails_env/images/avatars/:hash.:extension",
                    styles: { original: "170x170#", thumb: "50x50#" }






  validates :email, presence: true, uniqueness: true, email: true
  validates :second_email, email: true, allow_blank: true, if: -> { self.try(:second_email).present? }  # Quick fix for first migration where we create user,
                                                                                                        # but second_email attribute doesn't exists so it can't be validated
  validates :token, uniqueness: true, if: proc { |user| !user.token.blank? }
  validates :avatar, attachment_size: { less_than: 2.megabytes }
  validates_attachment_content_type :avatar, content_type: DOC_TYPES, message: ''
  validates_attachment_file_name :avatar, matches: [ Regexp.new("(#{DOC_EXT.join('|')})\\Z") ],
                                 message: "We support only #{DOC_EXT.join(', ')} files."

  def self.auth(email)
    user = User.find_by(email: email)
    if user
      user.token = Digest::MD5.hexdigest("#{email} #{Time.now}")
      return user if user.save
    else
      user = User.new(email: email, token: Digest::MD5.hexdigest("#{email} #{Time.now}"))
      if user.save
        user.set_role()
        return user
      end

    end
    false
  end

  def avatar_full_url
    self.avatar.present? ? URI.join(Roadmapbackend::Application.config.backend_host_url, self.avatar.url).to_s : nil
  end

  def avatar_thumb_full_url
    self.avatar.present? ? URI.join(Roadmapbackend::Application.config.backend_host_url, self.avatar.url(:thumb)).to_s : nil
  end


  def safe_destroy
    roadmaps = Roadmap.includes(:user_roadmaps).where(owner: self)
    roadmaps.each do |roadmap|
       if roadmap.user_roadmaps.count > 1
          UserRoadmap.where(roadmap_id: roadmap.id).update_all({ rights: UserRoadmap::USER_RIGHTS[:admin] })
        else
          roadmap.destroy
       end
    end
    self.destroy
  end

  def set_role(role_id = nil)
    self.roles = []
    if role_id == Role.find_by(name: :Admin).id
      self.add_role(:Admin)
    else
      self.add_role(:Member)
    end
  end
end