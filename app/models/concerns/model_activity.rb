module Concerns::ModelActivity
  extend ActiveSupport::Concern

  #class ActivityAuthorNotPresentedError < StandardError
  #end

  included do
    attr_accessor :activity_author
    attr_accessor :track_new_record

    around_save :track_changes
    around_destroy :track_changes
  end

  def track_changes
    model_changes = self.prepare_changes
    yield
    model_changes = self.after_prepare_changes(model_changes)
    create_activity(model_changes) if model_changes and model_changes.length > 0
  end

  def prepare_changes
    self.track_new_record = true if new_record?
    self.changes.inject({}) do |changes_list, attr_details|
      attr, attr_changes = attr_details

      if belongs = attr.match(/\A(.*?)_id\Z/)
        relation_name = belongs.captures.first
        if attr_presentation = self.send(relation_name.to_sym).try(:present_for_activity)
          attr_changes.map! do |attr_id|
            if attr_id
              self.send(relation_name.to_sym).class.find(attr_id).present_for_activity
            else
              attr_id
            end
          end
          changes_list.update(relation_name => attr_changes)
        else
          changes_list.update(attr => attr_changes)
        end
      else
        changes_list.update(attr => attr_changes)
      end
      changes_list
    end
  end

  def after_prepare_changes(model_changes)
    if self.track_new_record
      model_changes.update('id' => [nil, self.id])
      self.track_new_record = false
    end
    if self.destroyed?
      model_changes = {'id' => [self.id, nil]}
    end
    model_changes
  end

  def get_activity_roadmap
    nil
  end

  def present_for_activity
    {id: self.id}
  end

  def publish_to_redis(activity)

    channel_name = "roadmap_#{activity.roadmap_id}"
    data = {
      id: activity.id,
      entity_type: activity.entity_type,
      entity_details: activity.entity_details,
      deltas: activity.deltas,
      created_at: activity.created_at.to_i,
      user: {
          id: activity.user.id,
          email: activity.user.email
      }
    }
    $redis.publish channel_name, data.to_json if $redis.connected?

    if activity.entity_type == UserRoadmap.name
      channel_name = "user_#{activity.entity_details[:user][:id]}"
      $redis.publish channel_name, data.to_json if $redis.connected?
    end
  end

  def create_activity(model_changes, roadmap_id=nil)
    if activity_author
      activity = Activity.create(
        entity_type: self.class.name,
        entity_details: self.present_for_activity,
        deltas: model_changes,
        user_id: self.activity_author.try(:id),
        roadmap_id: roadmap_id || self.get_activity_roadmap
      )
      recipients = activity.recipients
      NotificationsMailer.delay.activity(activity, recipients) if recipients.length > 0
      publish_to_redis activity
    end
  end
end