module Concerns::HashedModel
  extend ActiveSupport::Concern

  def hashed_id
    hash_ids = Hashids.new(Roadmapbackend::Application.config.hashids_salt_main)
    hash_ids.encrypt([self.id, self.created_at.to_i])
  end

  module ClassMethods

    def find_by_hash(hash)
      hash_ids = Hashids.new(Roadmapbackend::Application.config.hashids_salt_main)
      data = hash_ids.decrypt(hash)
      self.find(data[0])
    end

  end

end