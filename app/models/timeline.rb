class Timeline < ActiveRecord::Base
  include Concerns::HashedModel
  include Concerns::ModelActivity

  attr_accessor :copied_from

  belongs_to :roadmap, inverse_of: :timelines
  has_many :milestones, dependent: :destroy, inverse_of: :timeline

  validates :title, presence: true
  validates :is_archive, inclusion: { in: [true, false] }

  scope :active, -> { where(is_archive: false) }
  scope :archive, -> { where(is_archive: true) }

  after_initialize :default_values

  accepts_nested_attributes_for :milestones

  def copy_to(roadmap)
    timeline = self.dup
    timeline.title += ' copy'
    timeline.copied_from = {timeline: self.present_for_activity, roadmap: self.roadmap.present_for_activity}
    timeline.roadmap_id = roadmap.id

    labels_map = map_labels_to_roadmap(roadmap)

    self.milestones.each do |milestone|
      milestone_copy = milestone.dup
      milestone_copy.label_id = labels_map[milestone_copy.label_id]
      milestone.sub_items.each do |sub_item|
        milestone_copy.sub_items << sub_item.dup
      end
      timeline.milestones << milestone_copy
    end
    timeline
  end

  def move_to(dest_roadmap)
    previous_roadmap_id = self.roadmap_id
    labels_map = map_labels_to_roadmap(dest_roadmap)

    self.roadmap_id = dest_roadmap.id
    self.milestones.each do |milestone|
      milestone.label_id = labels_map[milestone.label_id]
    end

    # Need to create additional activity about moved out timeline for previous roadmap
    changes_for_previous_roadmap = self.prepare_changes

    if save
      self.create_activity(changes_for_previous_roadmap, previous_roadmap_id)
      true
    else
      false
    end
  end

  def map_labels_to_roadmap(dest_roadmap)
    used_labels = self.roadmap.labels
    available_labels = dest_roadmap.labels
    used_labels.inject({}) do |map, used_label|
      map.update(used_label.id => available_labels.detect {|label| label.color_id == used_label.color_id}.id)
    end
  end

  def after_prepare_changes(model_changes)
    if self.copied_from
      {
        self: [self.copied_from[:timeline], self.present_for_activity],
        roadmap: [self.copied_from[:roadmap], self.roadmap.present_for_activity]
      }
    else
      super(model_changes)
    end
  end

  def get_activity_roadmap
    roadmap_id
  end

  def present_for_activity
    presentation = super
    presentation.update(title: self.title)
  end

  private
    def default_values
      self.is_archive ||= false
    end
end