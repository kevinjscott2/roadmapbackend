class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    if user.has_role?(:Admin)
      can :manage, :all
      cannot [:update, :destroy], UserRoadmap do |subject|
        !subject.new_record? && subject.user == subject.roadmap.owner
      end
    elsif user.has_role?(:Member)
      cannot :manage, :all

      can :create, User
      can :update, User, id: user.id
      can :destroy, User do |subject|
        (user.has_role? :Admin) ? true : subject.id == user.id
      end

      can [:update, :destroy], Roadmap, user_roadmaps: { user_id: user.id, rights: UserRoadmap::USER_RIGHTS[:admin] }
      can :read, Roadmap, user_roadmaps: { user_id: user.id }

      can :manage, Timeline, roadmap: { user_roadmaps: { user_id: user.id, rights: UserRoadmap::USER_RIGHTS[:admin] }}
      can :manage, Milestone, timeline: { roadmap: { user_roadmaps: { user_id: user.id, rights: UserRoadmap::USER_RIGHTS[:admin] }}}
      can :manage, SubItem, milestone: { timeline: { roadmap: { user_roadmaps: { user_id: user.id, rights: UserRoadmap::USER_RIGHTS[:admin] }}}}
      can :manage, Label, roadmap: { user_roadmaps: { user_id: user.id, rights: UserRoadmap::USER_RIGHTS[:admin] }}

      can :read, Timeline, roadmap: { user_roadmaps: { user_id: user.id }}
      can :read, Milestone, timeline: { roadmap: { user_roadmaps: { user_id: user.id }}}
      can :read, Label, roadmap: { user_roadmaps: { user_id: user.id }}

      can [:create, :update, :destroy], UserRoadmap do |subject|

        if !subject.new_record? && subject.user == subject.roadmap.owner
          false
        end

        if user.has_role? :Member
          user_roadmap = UserRoadmap.where(roadmap: subject.roadmap, user: user).first
          user_rights = user_roadmap ? user_roadmap.rights : nil
          user_rights == UserRoadmap::USER_RIGHTS[:admin] ||
              ((user_rights == UserRoadmap::USER_RIGHTS[:reader]) && (subject.rights == UserRoadmap::USER_RIGHTS[:reader]))
        end
      end
    end
    can :read, Roadmap, is_private: false, is_archive: false
  end
end