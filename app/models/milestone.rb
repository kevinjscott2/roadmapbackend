class Milestone < ActiveRecord::Base
  include Concerns::HashedModel
  include Concerns::ModelActivity

  belongs_to :label, inverse_of: :milestones
  belongs_to :timeline, inverse_of: :milestones
  has_many :sub_items, -> { order 'position' }, dependent: :destroy, inverse_of: :milestone

  validates :description, presence: true
  validates :timeline, presence: true
  validates :label, presence: true
  validates :date_end, date: true
  validate :only_available_labels

  before_validation :set_label

  accepts_nested_attributes_for :sub_items

  def self.get_list(roadmap_id, page = 0, per_page = 0)
    Milestone.joins(:timeline).where(timelines: { roadmap_id: roadmap_id }).order(date_end: :asc).page(page).per(per_page)
  end

  def prepare_changes
    if self.is_sub_items_resorted?
      { resorted: true }
    else
      super unless self.timeline.copied_from
    end
  end

  def after_prepare_changes(model_changes)
    self.is_sub_items_resorted? ?  model_changes.update(self.present_for_activity) : super
  end

  def get_activity_roadmap
    timeline.try(:get_activity_roadmap)
  end


  def present_for_activity
    presentation = super
    if self.is_sub_items_resorted?
      presentation.update(
          sub_items: self.sub_items.map do |item| item.id end,
          description: self.description,
          timeline: self.timeline.present_for_activity,
          hashed_id: self.hashed_id,
          date_end: self.date_end
      )
    else
      presentation.update(date_end: self.date_end, timeline: self.timeline.present_for_activity, hashed_id: self.hashed_id)
    end
  end

  def is_sub_items_resorted?
    return @is_sub_items_resorted
  end

  def mark_as_resorted
    @is_sub_items_resorted = true
    self.update(updated_at: Time.now)
  end

  def color_id
    label.color_id
  end

  def color_id=(color)
    @color_id = color
  end

  def set_label
    if @color_id
      self.label = self.timeline.roadmap.labels.detect {|label| label.color_id == @color_id }
    end
  end

  private
    @is_sub_items_resorted = false


    def only_available_labels
      if self.timeline.present? && self.label.present? && self.label.roadmap_id != self.timeline.roadmap_id
        self.errors[:label] << 'The label does not belong to the current road map'
      end
    end
end