class Activity < ActiveRecord::Base
  paginates_per 10

  belongs_to :user
  belongs_to :roadmap

  serialize :deltas
  serialize :entity_details

  default_scope do
    order created_at: :desc
  end

  def entity
    @_entity ||= EntityManager.get_entity(self)
  end

  def recipients
    recipients = roadmap.subscriptions.includes(:user).active.map do |subscription|
      subscription.user.second_email.presence || subscription.user.email
    end
    recipients.delete(user.second_email.presence || user.email)
    recipients
  end

  class EntityManager
    def self.get_entity(activity)
      "#{activity.class.name}::#{activity.entity_type}Entity".constantize.new(activity)
    end
  end

  class Entity < Struct.new(:activity)
    def base_url
      Roadmapbackend::Application.config.host_url
    end

    def deltas
      activity.deltas
    end

    def action
      if deltas['id']
        if deltas['id'].first == nil
          'created'
        elsif deltas['id'].last == nil
          'deleted'
        end
      end
    end

    def user
      activity.user.email
    end

    def roadmap
      {
          title: activity.roadmap.name,
          link: "#{base_url}/r/#{activity.roadmap.hashed_id}"
      }
    end

    def destination
    end

    def title
    end

    def permalink
    end

    def format
      [user, action, destination]
    end
  end

  class RoadmapEntity < Entity
    def action
      action_name = super
      unless action_name
        if deltas['name']
          action_name = "renamed current roadmap from #{deltas['name'].first}"
        elsif deltas['is_private']
          action_name = "marked current roadmap as #{deltas['is_private'].last ? 'private' : 'public'}"
        elsif deltas['is_archive']
          action_name = "moved current roadmap #{deltas['is_archive'].last ? 'to' : 'out from'} archive"
        end
      end
      if %w{created deleted}.include? action_name
        action_name += ' current roadmap'
      end
      action_name
    end

    def title
      activity.entity_details[:name]
    end

    def permalink
      "#{base_url}/r/#{activity.roadmap.hashed_id}"
    end
  end

  class TimelineEntity < Entity
    def action
      action_name = super
      unless action_name
        if deltas['title']
          action_name = "renamed timeline #{title} from #{deltas['title'].first}"
        elsif deltas['is_archive']
          action_name = "moved timeline #{title} #{deltas['is_archive'].last ? 'to' : 'out from'} archive"
        elsif deltas[:roadmap] && deltas[:self].blank?
          action_name = 'moved'
        elsif deltas[:roadmap] && deltas[:self]
          action_name = 'copied'
        end
      end
      if %w{created deleted}.include? action_name
        action_name += " timeline #{title}"
      end
      action_name
    end

    def title
      activity.entity_details[:title]
    end

    def destination
      dest = ''
      action_name = action
      if %w{moved copied}.include? action_name
        roadmap_from = deltas[:roadmap].first
        roadmap_to = deltas[:roadmap].last
        dest = ['timeline', title, 'from', {title: roadmap_from[:name], link: "#{base_url}/r/#{roadmap_from[:hashed_id]}"}, 'roadmap']
        if action_name == 'moved'
          dest += [
              'to',
              {title: roadmap_to[:name], link: "#{base_url}/r/#{roadmap_to[:hashed_id]}"},
              'roadmap'
          ]
        end
      end
      dest
    end
  end

  class MilestoneEntity < Entity
    def action
      action_name = super
      unless action_name
        if deltas['description']
          action_name = "changed description of #{title} from #{deltas['description'].first.truncate(100)}"
        elsif deltas['date_end']
          action_name = "changed date end of #{title} from #{deltas['date_end'].first}"
        elsif deltas[:resorted]
          action_name = "resort sub items"
        end
      end
      if %w{created deleted}.include? action_name
        action_name += " #{title}"
      end
      action_name
    end

    def title
      "milestone '#{activity.entity_details[:date_end]}'"
    end

    def permalink
      "#{base_url}/m/#{activity.entity_details[:hashed_id]}"
    end

    def destination
      if deltas[:resorted]
        [
            "at milestone #{activity.entity_details[:title]}",
            {
                title: title,
                link: permalink
            },
        ]
      else
        "at timeline #{activity.entity_details[:timeline][:title]}"
      end

    end
  end

  class LabelEntity < Entity
    def action
      action_name = super
      unless action_name
        if deltas['text']
          action_name = "renamed label #{title} from #{deltas['text'].first}"
        end
      end
      action_name
    end

    def title
      activity.entity_details[:text]
    end

    def permalink
      "#{base_url}/r/#{activity.roadmap.hashed_id}"
    end

    def destination
      ''
    end
  end

  class SubItemEntity < Entity
    def action
      action_name = super
      unless action_name
        if deltas['name']
          action_name = "renamed sub item #{title} from #{deltas['name'].first}"
        end
      end
      if %w{created deleted}.include? action_name
        action_name += " #{title}"
      end
      action_name
    end

    def title
      activity.entity_details[:name]
    end

    def permalink
      "#{base_url}/m/#{activity.entity_details[:milestone][:hashed_id]}"
    end

    def destination
      [
        'at milestone',
        {
            title: activity.entity_details[:milestone][:date_end],
            link: permalink
        },
        'of timeline',
        activity.entity_details[:milestone][:timeline][:title]
      ]
    end
  end

  class UserRoadmapEntity < Entity
    def action
      action_name = super
      if action_name == 'created'
        action_name = "added #{activity.entity_details[:user][:email]} to"
      elsif action_name == 'deleted'
        action_name = "deleted #{activity.entity_details[:user][:email]} from"
      else
        role = activity.entity_details[:rights] == 'reader' ? 'a normal user' : 'an admin'
        action_name = "made #{activity.entity_details[:user][:email]} #{role} on"
      end
      action_name
    end

    def destination
      'this board'
    end
  end
end
