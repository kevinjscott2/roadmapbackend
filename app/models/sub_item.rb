class SubItem < ActiveRecord::Base
  include Concerns::ModelActivity

  belongs_to :milestone, inverse_of: :sub_items

  validates :name, presence: true
  validates :milestone, presence: true
  validates :position, numericality: { only_integer: true }, allow_blank: true

  after_create :init_position

  def prepare_changes
    super unless self.milestone.timeline.copied_from
  end

  def get_activity_roadmap
    milestone.try(:get_activity_roadmap)
  end

  def present_for_activity
    presentation = super
    presentation.update(name: self.name, milestone: self.milestone.present_for_activity)
  end

  private

    def init_position
      max_position = SubItem.where(milestone: self.milestone).maximum(:position) || 0
      self.update_columns(position: max_position + 1)
    end
end