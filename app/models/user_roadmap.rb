class UserRoadmap < ActiveRecord::Base
  include Concerns::ModelActivity

  USER_RIGHTS = {
    admin: 0,
    reader: 1,
  }

  belongs_to :user
  belongs_to :roadmap

  validates :rights, presence: true, numericality: { only_integer: true }
  validates :user, presence: true
  validates :user_id, presence: true
  validates_uniqueness_of :user_id, scope: :roadmap_id

  after_create :init_user_subscription
  after_destroy :destroy_user_subscription

  def rights_title
    USER_RIGHTS.invert[rights].to_s
  end

  def get_activity_roadmap
    roadmap_id
  end

  def present_for_activity
    presentation = super
    presentation.update(user: {id: user.id, email: user.email}, rights: rights_title)
  end

  def init_user_subscription
    user.subscriptions.where(roadmap_id: roadmap_id).first_or_initialize.tap do |subscription|
      subscription.is_subscribed = false
      subscription.roadmap_id = self.roadmap_id
      subscription.save
    end
  end

  def destroy_user_subscription
    user.subscriptions.where(roadmap_id: roadmap_id).delete_all
  end
end