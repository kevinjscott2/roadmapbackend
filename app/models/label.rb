class Label < ActiveRecord::Base
  include Concerns::ModelActivity
  LABELS_LIST = %w(#34b27d #e09952 #cb4d4d #9933cc #4d77cb #dbdb57)

  belongs_to :roadmap, inverse_of: :labels
  has_many :milestones, inverse_of: :label

  validates :roadmap, presence: true
  validates :color_id, presence: true, numericality: { only_integer: true }

  def prepare_changes
    super unless new_record?
  end

  def get_activity_roadmap
    roadmap_id
  end

  def present_for_activity
    presentation = super
    presentation.update(text: self.text, color_id: self.color_id)
  end
end