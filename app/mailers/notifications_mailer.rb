class NotificationsMailer < ActionMailer::Base
  layout 'notifications'

  def activity(activity, recipients)
    @activity = activity

    mail(to: recipients, subject: "Notification on roadmap #{activity.entity.roadmap[:title]}") do |format|
      format.html
    end
  end

  def add_user(new_user, current_user)
    @new_user = new_user
    @current_user = current_user

    mail(to: new_user.email, subject: "You were added to ESPN Roadmap Tools") do |format|
      format.html
    end
  end

  def add_user_roadmap(user_roadmap, current_user)
    @user_roadmap = user_roadmap
    @current_user = current_user

    mail(to: user_roadmap.user.email, subject: "You were added to #{user_roadmap.roadmap.name} roadmap") do |format|
      format.html
    end
  end
end