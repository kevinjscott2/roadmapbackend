class HomeController < ApplicationController
  def index
    render inline: 'Home page'
  end
end