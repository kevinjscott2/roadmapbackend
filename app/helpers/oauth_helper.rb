module OauthHelper
  class UnknownOauthProviderError < ArgumentError
  end

  def self.get_user_email(provider, access_token)
    provider = "#{self.name}::#{provider.classify}"
    unless OauthProvider.descendants.map(&:name).include? provider
      raise UnknownOauthProviderError, 'Unknown OAuth provider'
    end
    provider.constantize.get_user_email(access_token)
  end

  class OauthProvider
    def self.api_url
      raise NotImplementedError
    end

    def self.get_user_email(access_token)
      response = HTTParty.get(self.api_url, query: {
          access_token: access_token,
          fields: :email
      }, headers: {
          'User-Agent' => 'Mozilla/Firefox'
      })

      response.code == 200 ? JSON.parse(response.body)['email'] : nil
    end
  end

  class Facebook < OauthProvider
    def self.api_url
      'https://graph.facebook.com/v2.0/me'
    end
  end

  class Google < OauthProvider
    def self.api_url
      'https://www.googleapis.com/oauth2/v2/tokeninfo'
    end
  end

  class Github < OauthProvider
    def self.api_url
      Roadmapbackend::Application.config.github_api_url
    end
  end
end