module NotificationsMailerHelper
  def format_activity(details)
    details.compact.map do |item|
      if item.is_a? Array
        self.format_activity(item)
      elsif item.is_a? Hash
        #link_to(item[:title], item[:link]).html_safe
        "<a href=\"#{item[:link]}\" target=\"_blank\">#{item[:title]}</a>".html_safe
      else
        item.to_s
      end
    end.join(' ').html_safe
  end
end