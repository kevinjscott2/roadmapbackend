module SharedParams
  extend Grape::API::Helpers

  params :pagination do
    optional :page, type: Integer, default: 1
    optional :per_page, type: Integer, default: 20
  end

  params :api_token do
    requires :token, type: String, desc: 'API auth token'
  end
end