module RoadMap
  class Roadmaps < API
    resource :roadmaps do

      helpers SharedParams



      desc 'Return roadmap info', {
          entity: Entities::Roadmap
      }
      params do
        optional :token, type: String, desc: 'API auth token'
        requires :hash, type: String, desc: 'Roadmap uniq hash'
      end
      get do
        roadmap = Roadmap.find_by_hash(params[:hash])
        authorize!(:read, roadmap)
        present roadmap, with: Entities::Roadmap, user: current_user
      end



      desc 'Return roadmap info by milestone', {
          entity: Entities::Roadmap
      }
      params do
        optional :token, type: String, desc: 'API auth token'
        requires :milestone_hash, type: String, desc: 'Milestone uniq hash'
      end

      get :by_milestone do
        milestone = Milestone.find_by_hash(params[:milestone_hash])
        roadmap = Roadmap.find(milestone.timeline.roadmap_id)
        authorize!(:read, roadmap)
        present roadmap, with: Entities::Roadmap, user: current_user
      end



      desc 'Return active roadmaps list', {
          entity: Entities::Roadmap
      }
      params do
        use :api_token
        use :pagination
      end
      get :active do
          present Roadmap.active.get_available(current_user).page(params[:page]).per(params[:per_page]),
                  with: Entities::Roadmap, is_member: current_user.has_role?(:Member), user_subscriptions: current_user.subscriptions
      end


      desc 'Return archived roadmaps list', {
          entity: Entities::Roadmap,
      }
      params do
        use :api_token
        use :pagination
      end
      get :archive do
        present Roadmap.archive.get_available(current_user), with: Entities::Roadmap, user_subscriptions: current_user.subscriptions
      end



      desc 'Create new roadmap', {
          http_codes: [
              [422, 'Unprocessable Entity. (Not valid data)']
          ]
      }
      params do
        use :api_token
        requires :name, type: String, desc: 'Roadmap name'
      end
      post do
        roadmap = Roadmap.new(name: params[:name], owner: current_user)
        if roadmap.save
          # Tried to move this statement to model after_create, but it fails specs. Seeding of db becomes not the same as earlier
          roadmap.user_roadmaps.create(
              user_id: roadmap.owner_id,
              rights: UserRoadmap::USER_RIGHTS[:admin]
          )
          roadmap.seed_test_data
          {
            id: roadmap.id,
            hash: roadmap.hashed_id
          }
        else
          error!({ errors: roadmap.errors }, 422)
        end
      end



      desc 'Update roadmap', {
          http_codes: [
              [422, 'Unprocessable Entity. (Not valid data)']
          ]
      }
      params do
        use :api_token
        requires :id, type: Integer, desc: 'Roadmap ID'
        requires :name, type: String, desc: 'Name'
        requires :is_archive, type: Boolean, desc: 'Archive status'
        requires :is_private, type: Boolean, desc: 'Private status'
      end

      put ':id' do
        roadmap = Roadmap.find(params[:id])
        authorize!(:update, roadmap)
        roadmap.activity_author = current_user
        roadmap.update(name: params[:name], is_archive: params[:is_archive], is_private: params[:is_private])
        if roadmap.valid?
          {
              id: roadmap.id
          }
        else
          error!({ errors: roadmap.errors }, 422)
        end
      end



      desc 'Delete roadmap'
      params do
        use :api_token
        requires :id, type: Integer, desc: 'Roadmap ID'
      end
      delete ':id' do
        roadmap = Roadmap.archive.find(params[:id])
        authorize!(:destroy, roadmap)
        roadmap.destroy
        {
            id: params[:id]
        }
      end

      desc 'Export roadmap info', {
          entity: Entities::Roadmap
      }
      params do
        optional :token, type: String, desc: 'API auth token'
        requires :hash, type: String, desc: 'Roadmap uniq hash'
      end
      get 'export' do
        roadmap = Roadmap.find_by_hash(params[:hash])
        authorize!(:read, roadmap)
        content_type 'application/json'
        header['Content-Disposition'] = "attachment; filename=Roadmap-#{roadmap.name}-#{Time.zone.now.strftime('%Y%m%d_%H%M%S')}-export.json"
        env['api.format'] = :binary
        present roadmap, with: Entities::Export::Roadmap
      end

      desc 'Import roadmap as a new roadmap', {
          http_codes: [
              [422, 'Unprocessable Entity. (Not valid data)']
          ]
      }
      params do
        optional :token, type: String, desc: 'API auth token'
        requires :file, type: Rack::Multipart::UploadedFile, desc: 'File that contains roadmap content represented in JSON format. See export method for more details.'
      end
      post 'import' do
        begin
          import_data = JSON::parse(params[:file].tempfile.read)
          raise unless import_data
          roadmap = Roadmap.new(import_data)
          roadmap.owner_id = current_user.id
          roadmap.import = true
          if roadmap.save
            UserRoadmap.create(user: current_user, roadmap: roadmap, rights: UserRoadmap::USER_RIGHTS[:admin])
            {
                id: roadmap.id,
                hash: roadmap.hashed_id
            }
          else
            error!({ errors: roadmap.errors }, 422)
          end
        rescue Exception
          error!('It seems that content of the file is not a proper JSON object or it has wrong format', 422)
        end
      end
    end
  end
end