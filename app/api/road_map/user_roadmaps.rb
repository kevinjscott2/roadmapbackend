module RoadMap
  class UserRoadmaps < API
    resource :user_roadmaps do

      helpers SharedParams

      desc 'Return list of rights', {
          entity: Entities::UserRoadmap
      }
      params do
        optional :token, type: String, desc: 'API auth token'
        requires :roadmap_id, type: Integer, desc: 'Roadmap ID'
      end
      get do
        roadmap = Roadmap.find(params[:roadmap_id])
        authorize!(:read, roadmap)
        present UserRoadmap.where(roadmap_id: roadmap.id), with: Entities::UserRoadmap
      end


      desc 'Assign user to roadmap', {
          http_codes: [
              [422, 'Unprocessable Entity. (Not valid data)']
          ]
      }
      params do
        use :api_token
        requires :user_id, type: String, desc: 'Added user ID'
        requires :roadmap_id, type: Integer, desc: 'Roadmap ID'
        requires :rights, type: Integer, desc: 'Type of rights'
      end
      post do
        user_roadmap = UserRoadmap.new(
            user_id: params[:user_id],
            roadmap_id: params[:roadmap_id],
            rights: params[:rights]
        )
        authorize!(:create, user_roadmap)
        user_roadmap.activity_author = current_user
        if user_roadmap.save
          NotificationsMailer.delay.add_user_roadmap(user_roadmap, current_user)
          {
              id: user_roadmap.id
          }
        else
          error!({ errors: user_roadmap.errors }, 422)
        end
      end


      desc 'Change access rights of the user already assigned to roadmap', {
          http_codes: [
              [422, 'Unprocessable Entity. (Not valid data)']
          ]
      }
      params do
        use :api_token
        requires :id, type: Integer, desc: 'UserRoadmap ID'
        requires :rights, type: Integer, desc: 'Type of rights'
      end
      put ':id' do
        user_roadmap = UserRoadmap.find(params[:id])
        user_roadmap.rights = params[:rights]
        authorize!(:update, user_roadmap)
        user_roadmap.activity_author = current_user
        if user_roadmap.save
          {
              id: user_roadmap.id
          }
        else
          error!({ errors: user_roadmap.errors }, 422)
        end
      end


      desc 'Unassign user from roadmap'
      params do
        use :api_token
        requires :id, type: Integer, desc: 'User_roadmap ID'
      end
      delete ':id' do
        user_roadmap = UserRoadmap.find(params[:id])
        authorize!(:destroy, user_roadmap)
        user_roadmap.activity_author = current_user
        user_roadmap.destroy
        {
            id: params[:id]
        }
      end
    end
  end
end