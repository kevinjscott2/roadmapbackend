module RoadMap
  class SubItems < Grape::API
    resource :sub_items do
      helpers SharedParams

      desc 'Create new sub-item', {
        http_codes: [
          [422, 'Unprocessable Entity. (Not valid data)']
        ]
      }
      params do
        use :api_token
        requires :name, type: String, desc: 'Text'
        requires :milestone_id, type: Integer, desc: 'Milestone ID'
      end
      post do
        sub_item = SubItem.new( name:  params[:name], milestone_id:  params[:milestone_id])
        authorize!(:manage, sub_item)
        sub_item.activity_author = current_user
        if sub_item.save
          {
              id: sub_item.id
          }
        else
          error!({ errors: sub_item.errors }, 422)
        end
      end

      desc 'Resort sub-items'
      params do
        use :api_token
        requires :milestone_id, type: String, desc: 'Milestone ID'
        requires :sorted_ids, type: String, desc: 'ID`s sorted according to the specified positions separated by commas'
      end
      put 'sort/:milestone_id' do
        milestone = Milestone.find(params[:milestone_id])
        authorize!(:manage, milestone)
        data = params[:sorted_ids].split(',')
        if data.count != SubItem.where(milestone: milestone, id: data).count
          error!({ errors: 'Wrong sub-item ids' }, 422)
        end

        data.each_with_index { |id, index|
          SubItem.where(id: id).update_all(position: index + 1)
        }
        milestone.activity_author = current_user
        milestone.mark_as_resorted
        {}
      end

      desc 'Rename sub item', {
        http_codes: [
            [422, 'Unprocessable Entity. (Not valid data)']
        ]
      }
      params do
        use :api_token
        requires :id, type: Integer, desc: 'Sub item ID'
        requires :name, type: String, desc: 'New name for sub item'
      end
      put ':id' do
        sub_item = SubItem.find(params[:id])
        authorize!(:manage, sub_item)

        sub_item.activity_author = current_user

        if sub_item.update_attributes(name: params[:name])
          present sub_item, with: Entities::SubItem
        else
          error!({errors: sub_item.errors}, 422)
        end
      end

      desc 'Delete sub-item'
      params do
        use :api_token
        requires :id, type: Integer, desc: 'Sub-item ID'
      end
      delete ':id' do
        sub_item = SubItem.find(params[:id])
        authorize!(:manage, sub_item)
        sub_item.activity_author = current_user
        sub_item.destroy
        {
            id: params[:id]
        }
      end
    end
  end
end