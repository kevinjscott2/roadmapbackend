module RoadMap
  class Users < Grape::API
    resource :users do
      helpers SharedParams

      desc 'Return API auth token', {
          entity: Entities::AuthToken,
          http_codes: [
              [404, 'Invalid google token or user not admin']
          ]
      }
      params do
        requires :provider, type: String, desc: 'OAuth provider name. Can be google, facebook, github'
        requires :access_token, type: String, desc: 'OAuth access token'
      end

      post :auth do
        begin
          user_email = OauthHelper.get_user_email(params[:provider], params[:access_token])
          user = User.auth(user_email)
          if user
            present user, with: Entities::AuthToken
          else
            raise ActiveRecord::RecordNotFound
          end
        rescue OauthHelper::UnknownOauthProviderError
          error!('Unknown OAuth provider specified. Should be facebook, google or github', 400)
        end
      end

      desc 'Return list of users', {
          entity: Entities::User,
      }
      params do
        use :api_token
        use :pagination
      end
      get do
        authorize!(:read, User)
        present User.page(params[:page]).per(params[:per_page]), with: Entities::User
      end

      desc 'Search user by email', {
          entity: Entities::User,
      }
      params do
        use :api_token
        requires :email, type: String, desc: 'Search email text'
        optional :member_only, type: Boolean, desc: 'For only members date'
      end
      get :search do

        if params[:member_only]
          present User.joins(:roles).where(roles: {name: :Member}).search(email_cont: params[:email]).result.limit(10),
                  with: Entities::User
        else
          present User.search(email_cont: params[:email]).result.limit(10), with: Entities::User
        end
      end

      desc 'Create new user', {
          http_codes: [
              [422, 'Unprocessable Entity. (Not valid data)']
          ]
      }
      params do
        use :api_token
        requires :email, type: String, desc: 'Email'
        optional :role_id, type: Integer, desc: 'Role ID'
      end
      post do
        authorize!(:create, User)
        if current_user.has_role?(:Admin)
          user = User.create(email:  params[:email])
          user.set_role(params[:role_id])
        elsif current_user.has_role?(:Member)
          user = User.create(email:  params[:email])
          user.set_role()
        end

        if user.valid?
          NotificationsMailer.delay.add_user(user, current_user)
          {
              id: user.id
          }
        else
          error!({ errors: user.errors }, 422)
        end
      end

      desc 'Update user', {
          http_codes: [
              [422, 'Unprocessable Entity. (Not valid data)']
          ]
      }
      params do
        use :api_token
        requires :id, type: Integer, desc: 'User ID'
        requires :email, type: String, desc: 'Email'
        optional :second_email, type: String, desc: 'Email for notifications'
        requires :role_id, type: Integer, desc: 'Role ID'
      end

      put ':id' do
        user = User.find(params[:id])
        authorize!(:update, user)
        user.update(email:  params[:email])
        user.set_role(params[:role_id])

        if user.valid?
          {
              id: user.id
          }
        else
          error!({ errors: user.errors }, 422)
        end
      end


      desc 'Upload avatar image'
      params do
        use :api_token
        requires :avatar, type: Rack::Multipart::UploadedFile, desc: 'Avatar image'
      end

      post 'upload_avatar' do
        authorize!(:update, current_user)
        current_user.avatar = params[:avatar]

        if current_user.save
          present current_user, with: Entities::AuthToken
        else
          error!({ errors: current_user.errors }, 422)
        end
      end

      desc 'Delete user'
      params do
        use :api_token
        requires :id, type: Integer, desc: 'User ID'
      end
      delete ':id' do
        user = User.find(params[:id])
        authorize!(:destroy, user)
        if user.safe_destroy
          {
            id: params[:id]
          }
        else
          error!({ errors: user.errors }, 422)
        end
      end


      desc 'Return roles list', {
          entity: Entities::Role
      }
      get :roles do
        present Role.all, with: Entities::Role
      end

      desc 'Return rights list'
      get :rights do
        UserRoadmap::USER_RIGHTS
      end
    end
  end
end