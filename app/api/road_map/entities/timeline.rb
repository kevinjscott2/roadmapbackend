module RoadMap::Entities
  class Timeline < Grape::Entity
    expose :id, documentation: { type: 'Integer', desc: 'ID' }
    expose :title, documentation: { type: 'String', desc: 'Title' }
    expose :is_archive, documentation: { type: 'Boolean', desc: 'Archive status' }
    expose :roadmap_id, documentation: { type: 'Integer', desc: 'Roadmap ID' }
  end
end
