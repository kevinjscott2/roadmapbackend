module RoadMap::Entities
  class Milestone < Grape::Entity
    expose :id, documentation: { type: 'Integer', desc: 'ID' }
    expose :description, documentation: { type: 'Text', desc: 'Description' }
    expose :hashed_id, as: :hash, documentation: { type: 'String', desc: 'Uniq hash' }
    expose :date_end, documentation: { type: 'Timestamp', desc: 'Timestamp of date end' } do |item|
      item.date_end
    end
    expose :timeline_id, documentation: { type: 'Integer', desc: 'ID of timeline' }
    expose :label_id
    expose :sub_items, using: SubItem
  end
end
