module RoadMap::Entities
  class SubItem < Grape::Entity
    expose :id, documentation: { type: 'Integer', desc: 'ID' }
    expose :name, documentation: { type: 'String', desc: 'Name' }
    expose :position, documentation: { type: 'Integer', desc: 'Item order position' }
  end
end
