module RoadMap::Entities
  class AuthToken < Grape::Entity
    expose :id, documentation: { type: 'Integer', desc: 'User ID' }
    expose :avatar, documentation: { type: 'String', desc: 'Link to avatar image' } do |model|
      model.avatar_full_url
    end
    expose :avatar_thumb, documentation: { type: 'String', desc: 'Link to thumb avatar image' } do |model|
      model.avatar_thumb_full_url
    end
    expose :email, documentation: { type: 'String', desc: 'Auth user email' }
    expose :second_email, documentation: { type: 'String', desc: 'email for notifications' }
    expose :token, documentation: { type: 'String', desc: 'Auth user API token' }
    expose :role, documentation: { type: 'Integer', desc: 'Role ID' } do |model|
      model.roles.first.id
    end
  end
end
