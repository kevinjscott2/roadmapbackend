module RoadMap::Entities
  class Role < Grape::Entity
    expose :id, documentation: { type: 'Integer', desc: 'ID' }
    expose :name, documentation: { type: 'Integer', desc: 'Role name' }
  end
end