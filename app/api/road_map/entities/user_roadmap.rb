module RoadMap::Entities
  class UserRoadmap < Grape::Entity
    expose :id, documentation: { type: 'Integer', desc: 'ID' }
    expose :roadmap_id, documentation: { type: 'Integer', desc: 'ID of Roadmap' }
    expose :rights
    expose :user, using: User

  end
end
