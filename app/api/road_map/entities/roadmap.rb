module RoadMap::Entities
  class Roadmap < Grape::Entity
    expose :id, documentation: { type: 'Integer', desc: 'ID' }
    expose :name, documentation: { type: 'String', desc: 'Name' }
    expose :hashed_id, as: :hash, documentation: { type: 'String', desc: 'Uniq hash' }
    expose :is_private, documentation: { type: 'Boolean', desc: 'Private status' } do |model|
      !!model.is_private
    end
    expose :owner_id
    expose :rights, if: { is_member: true }, documentation: { type: 'Integer', desc: 'Auth member rights' } do |model|
      model.user_roadmaps.first.rights
    end

    expose :is_subscribed, documentation: { type: 'Boolean', desc: 'Tells if current user subscribed on email notifications of roadmap' } do |model, options|
      if options[:user].present?
        !!model.subscriptions.where(user_id: options[:user].id).take.try(:is_subscribed)
      elsif options[:user_subscriptions].present?
        !!options[:user_subscriptions].detect { |sub| sub.roadmap_id == model.id }.try(:is_subscribed)
      else
        false
      end
    end
  end
end