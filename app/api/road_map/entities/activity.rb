module RoadMap::Entities
  class Activity < Grape::Entity
    expose :id, documentation: { type: 'Integer', desc: 'ID' }
    expose :entity_type, documentation: { type: 'String', desc: 'Entity type name Milestone, Roadmap, Timeline, SubItem, UserRoadmap' }
    expose :entity_details, documentation: { type: 'Hash', desc: 'Distinctive attributes of the entity' }
    expose :deltas, documentation: { type: 'Hash', desc: 'Diff of changes made under object' }
    expose :created_at, documentation: { type: 'Timestamp', desc: 'Timestamp of created at' } do |item|
      item.created_at.to_i
    end
    expose :user, using: User
  end
end
