module RoadMap::Entities
  class Export::Label < Grape::Entity
    expose :color_id, documentation: { type: 'Integer', desc: 'Color identifier' }
    expose :text, documentation: { type: 'String', desc: 'Label description' }
  end
end
