module RoadMap::Entities
  class Export::Roadmap < Grape::Entity
    expose :name, documentation: { type: 'String', desc: 'Name' }
    expose :is_private, documentation: { type: 'Boolean', desc: 'Private status' } do |model|
      !!model.is_private
    end
    expose :is_archive, documentation: { type: 'Boolean', desc: 'Archive status' } do |model|
      !!model.is_archive
    end
    expose :created_at, documentation: { type: 'Timestamp', desc: 'When roadmap was created' }
    expose :updated_at, documentation: { type: 'Timestamp', desc: 'When was last update of roadmap' }
    expose :timelines, as: :timelines_attributes, using: Export::Timeline
    expose :labels, as: :labels_attributes, using: Export::Label
  end
end