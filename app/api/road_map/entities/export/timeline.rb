module RoadMap::Entities
  class Export::Timeline < Grape::Entity
    expose :title, documentation: { type: 'String', desc: 'Title' }
    expose :is_archive, documentation: { type: 'Boolean', desc: 'Archive status' }
    expose :created_at, documentation: { type: 'Timestamp', desc: 'When timeline was created' }
    expose :updated_at, documentation: { type: 'Timestamp', desc: 'When was last update of timeline' }
    expose :milestones, as: :milestones_attributes, using: Export::Milestone
  end
end
