module RoadMap::Entities
  class Export::SubItem < Grape::Entity
    expose :name, documentation: { type: 'String', desc: 'Name' }
    expose :position, documentation: { type: 'Integer', desc: 'Item order position' }
    expose :created_at, documentation: { type: 'Timestamp', desc: 'When sub item was created' }
  end
end
