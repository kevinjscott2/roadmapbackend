module RoadMap::Entities
  class Export::Milestone < Grape::Entity
    expose :description, documentation: { type: 'Text', desc: 'Description' }
    expose :date_end, documentation: { type: 'Timestamp', desc: 'Timestamp of date end' } do |item|
      item.date_end
    end
    expose :color_id, documentation: { type: 'Integer', desc: 'Color index from available colors array: [#34b27d, #e09952, #cb4d4d, #9933cc, #4d77cb, #dbdb57]. Index starting from 0' }
    expose :created_at, documentation: { type: 'Timestamp', desc: 'When milestone was created' }
    expose :updated_at, documentation: { type: 'Timestamp', desc: 'When was last update of milestone' }
    expose :sub_items, as: :sub_items_attributes, using: Export::SubItem
  end
end
