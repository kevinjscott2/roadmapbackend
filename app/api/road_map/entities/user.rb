module RoadMap::Entities
  class User < Grape::Entity
    expose :id, documentation: { type: 'Integer', desc: 'ID' }
    expose :email, documentation: { type: 'String', desc: 'email' }
    expose :second_email, unless: ->(instance, options) { options[:short_description] }, documentation: { type: 'String', desc: 'email for notifications' }
    expose :role, unless: ->(instance, options) { options[:short_description] }, documentation: { type: 'Integer', desc: 'Role ID' } do |model|
      model.roles.first.id
    end
    expose :avatar, documentation: { type: 'String', desc: 'Link to avatar image' } do |model|
      model.avatar_full_url
    end
    expose :avatar_thumb, documentation: { type: 'String', desc: 'Link to thumb avatar image' } do |model|
      model.avatar_thumb_full_url
    end
  end
end
