module RoadMap::Entities
  class Label < Grape::Entity
    expose :id, documentation: { type: 'Integer', desc: 'ID' }
    expose :color_id, documentation: { type: 'Integer', desc: 'Color identifier' }
    expose :roadmap_id, documentation: { type: 'Integer', desc: 'Roadmap ID' }
    expose :text, documentation: { type: 'String', desc: 'Label description' }
  end
end
