module RoadMap::Entities
  class Subscription < Grape::Entity
    expose :roadmap_id, documentation: { type: 'Integer', desc: 'Roadmap id' }
    expose :is_subscribed, documentation: { type: 'Boolean', desc: 'Tells if current user subscribed on email notifications of roadmap' }
  end
end