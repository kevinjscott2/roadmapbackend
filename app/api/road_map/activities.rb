module RoadMap
  class Activities < API
    resource :activities do
      helpers SharedParams

      desc 'Return activities list for roadmap', {
          entity: Entities::Activity,
      }
      params do
        use :pagination
        optional :token, type: String, desc: 'API auth token'
        requires :roadmap_id, type: Integer, desc: 'Roadmap ID'
      end
      get do
        roadmap = Roadmap.find(params[:roadmap_id])
        authorize!(:read, roadmap)

        present roadmap.activities.page(params[:page]).per(params[:per_page]), with: Entities::Activity, short_description: true
      end
    end
  end
end