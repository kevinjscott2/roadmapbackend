module RoadMap
  class Subscriptions < API
    resource :subscriptions do
      helpers SharedParams

      desc 'Return list of current user\'s subscriptions', {
          entity: Entities::Subscription,
      }
      params do
        use :api_token
        use :pagination
      end
      get do
        present current_user.subscriptions.page(params[:page]).per(params[:per_page]), with: Entities::Subscription
      end

      desc 'Subscribe user on email notifications of roadmap', {

      }
      params do
        use :api_token
        requires :roadmap_id, type: Integer, desc: 'Roadmap ID'
      end
      post do
        roadmap = Roadmap.find(params[:roadmap_id])
        authorize!(:read, roadmap)

        subscription = current_user.subscriptions.where(roadmap_id: roadmap.id).first_or_initialize.tap do |sub|
          sub.is_subscribed = true
          sub.roadmap_id = roadmap.id
          sub.save
        end
        present subscription, with: Entities::Subscription
      end

      desc 'Unsubscribe user from email notifications of roadmap', {

      }
      params do
        use :api_token
        requires :roadmap_id, type: Integer, desc: 'Roadmap ID'
      end
      delete do
        roadmap = Roadmap.find(params[:roadmap_id])
        authorize!(:read, roadmap)

        current_user.subscriptions.where(roadmap_id: roadmap.id).update_all(is_subscribed: false)
        {
            roadmap_id: roadmap.id,
            is_subscribed: false
        }
      end
    end
  end
end