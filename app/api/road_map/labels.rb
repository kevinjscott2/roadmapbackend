module RoadMap
  class Labels < API
    resource :labels do
      helpers SharedParams


      desc 'Return list of label colors', {
          notes: 'List of hex css colors for label. the first and last colors correspond to indices(color_id) 0 .. 5'
      }
      get :colors do
        Label::LABELS_LIST
      end

      desc 'Return labels list of roadmap', {
          entity: Entities::Label,
      }
      params do
        optional :token, type: String, desc: 'API auth token'
        requires :roadmap_id, type: String, desc: 'Uniq roadmap hash ID'
      end
      get do
        roadmap = Roadmap.find(params[:roadmap_id])
        authorize!(:read, roadmap)
        present Label.where(roadmap_id: params[:roadmap_id]), with: Entities::Label
      end

      desc 'Update label', {
          http_codes: [
              [422, 'Unprocessable Entity. (Not valid data)']
          ],
      }
      params do
        use :api_token
        requires :id, type: Integer, desc: 'Label ID'
        requires :roadmap_id, type: Integer, desc: 'Roadmap ID'
        requires :text, type: String, desc: 'Text'
      end

      put ':id' do
        label = Label.where(roadmap_id: params[:roadmap_id]).find(params[:id])
        authorize!(:update, label)
        label.activity_author = current_user
        label.update(text:  params[:text])
        if label.valid?
          {
              id: label.id
          }
        else
          error!({ errors: label.errors }, 422)
        end
      end
    end
  end
end