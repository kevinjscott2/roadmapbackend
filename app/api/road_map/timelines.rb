module RoadMap
  class Timelines < API
    resource :timelines do
      helpers SharedParams



      desc 'Return timelines list', {
          entity: Entities::Timeline
      }
      params do
        optional :token, type: String, desc: 'API auth token'
        requires :roadmap_id, type: Integer, desc: 'Roadmap ID'
      end
      get do
        authorize!(:read, Roadmap.find(params[:roadmap_id]))
        roadmap = Roadmap.find(params[:roadmap_id])
        present Timeline.joins(:roadmap).where(roadmap_id: roadmap.id), with: Entities::Timeline
      end



      desc 'Create new timeline', {
          http_codes: [
              [422, 'Unprocessable Entity. (Not valid data)']
          ]
      }
      params do
        use :api_token
        requires :title, type: String, desc: 'Timeline title'
        requires :roadmap_id, type: Integer, desc: 'Roadmap ID'
      end
      post do
        timeline = Timeline.new(title: params[:title], roadmap_id: params[:roadmap_id])

        authorize!(:manage, timeline)
        timeline.activity_author = current_user
        if timeline.save
          {
            id: timeline.id,
          }
        else
          error!({ errors: timeline.errors }, 422)
        end
      end



      desc 'Update timeline', {
          http_codes: [
              [422, 'Unprocessable Entity. (Not valid data)']
          ]
      }
      params do
        use :api_token
        requires :id, type: Integer, desc: 'Timeline ID'
        requires :title, type: String, desc: 'Timeline title'
        requires :roadmap_id, type: Integer, desc: 'Roadmap ID'
        requires :is_archive, type: Boolean, desc: 'Archive status'
      end

      put ':id' do
        timeline = Timeline.find(params[:id])
        authorize!(:manage, timeline)
        timeline.activity_author = current_user
        timeline.update(title: params[:title], is_archive: params[:is_archive], roadmap_id: params[:roadmap_id])
        if timeline.valid?
          {
            id: timeline.id
          }
        else
          error!({ errors: timeline.errors }, 422)
        end
      end



      desc 'Delete timeline'
      params do
        use :api_token
        requires :id, type: Integer, desc: 'Timeline ID'
      end
      delete ':id' do
        timeline = Timeline.archive.find(params[:id])
        timeline.activity_author = current_user
        authorize!(:manage, timeline)
        timeline.destroy
        {
            id: params[:id]
        }
      end

      desc 'Copy timeline to roadmap', {
          http_codes: [
              [422, 'Unprocessable Entity. (Not valid data)']
          ]
      }
      params do
        use :api_token
        requires :id, type: Integer, desc: 'Timeline ID'
        requires :roadmap_id, type: Integer, desc: 'Roadmap ID'
      end

      post ':id/copy' do
        timeline = Timeline.find(params[:id])
        roadmap = Roadmap.where(id: params[:roadmap_id]).take

        unless roadmap.present?
          error!('Roadmap not found', 404)
        end

        authorize!(:update, roadmap)
        authorize!(:manage, timeline)

        timeline_copy = timeline.copy_to(roadmap)
        timeline_copy.activity_author = current_user
        if timeline_copy.save
          {
              id: timeline_copy.id
          }
        else
          error!({ errors: timeline_copy.errors }, 422)
        end
      end

      desc 'Move timeline to roadmap', {
          http_codes: [
              [422, 'Unprocessable Entity. (Not valid data)']
          ]
      }
      params do
        use :api_token
        requires :id, type: Integer, desc: 'Timeline ID'
        requires :roadmap_id, type: Integer, desc: 'Roadmap ID'
      end

      put ':id/move' do
        timeline = Timeline.find(params[:id])
        roadmap = Roadmap.where(id: params[:roadmap_id]).take

        unless roadmap.present?
          error!('Roadmap not found', 404)
        end

        authorize!(:update, roadmap)
        authorize!(:manage, timeline)

        timeline.activity_author = current_user
        if timeline.move_to(roadmap)
          {
              id: timeline.id
          }
        else
          error!({ errors: timeline.errors }, 422)
        end
      end
    end
  end
end