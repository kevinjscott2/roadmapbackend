module RoadMap

  class Milestones < Grape::API
    resource :milestones do

      helpers SharedParams

      desc 'Return milestones list of timeline', {
          entity: Entities::Milestone,
      }
      params do
        use :pagination
        optional :token, type: String, desc: 'API auth token'
        requires :roadmap_id, type: Integer, desc: 'Roadmap ID'
      end
      get do
        roadmap = Roadmap.find(params[:roadmap_id])
        authorize!(:read, roadmap)
        present Milestone.get_list(params[:roadmap_id], params[:page], params[:per_page]), with: Entities::Milestone
      end

      desc 'Create new milestone', {
          http_codes: [
              [422, 'Unprocessable Entity. (Not valid data)']
          ]
      }
      params do
        use :api_token
        requires :description, type: String, desc: 'Description'
        requires :timeline_id, type: Integer, desc: 'Timeline ID'
        requires :label_id, type: Integer, desc: 'Label ID'
        requires :date_end, type: Date, desc: 'End date'
      end
      post do
        milestone = Milestone.new(
            description:  params[:description],
            timeline_id:  params[:timeline_id],
            label_id:     params[:label_id],
            date_end:     params[:date_end]
        )
        milestone.activity_author = current_user
        authorize!(:manage, milestone)
        if milestone.save
          {
              id: milestone.id
          }
        else
          error!({ errors: milestone.errors }, 422)
        end
      end

      desc 'Update milestone', {
          http_codes: [
              [422, 'Unprocessable Entity. (Not valid data)']
          ],
      }
      params do
        use :api_token
        requires :id, type: Integer, desc: 'Milestone ID'
        requires :description, type: String, desc: 'Description'
        requires :label_id, type: Integer, desc: 'Label ID'
        requires :date_end, type: Date, desc: 'End date'
      end

      put ':id' do
        milestone = Milestone.find(params[:id])
        authorize!(:manage, milestone)
        milestone.activity_author = current_user
        milestone.update(
            description:  params[:description],
            label_id:     params[:label_id],
            date_end:     params[:date_end]
        )
        if milestone.valid?
          {
              id: milestone.id
          }
        else
          error!({ errors: milestone.errors }, 422)
        end
      end

      desc 'Delete milestone'
      params do
        use :api_token
        requires :id, type: Integer, desc: 'Milestone ID'
      end
      delete ':id' do
        milestone = Milestone.find(params[:id])
        authorize!(:manage, milestone)
        milestone.activity_author = current_user
        milestone.destroy
        {
          id: params[:id]
        }
      end
    end
  end
end