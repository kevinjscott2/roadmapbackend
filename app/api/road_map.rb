require 'grape-swagger'
module RoadMap
  class API < Grape::API
    VERSION = 'v1'

    version VERSION, using: :path
    format :json


    before do
      error!('401 Unauthorized', 401) unless authenticated? || params[:token].blank?
    end

    helpers do

      def current_user
        @_current_user ||= authenticate_user_from_token
      end

      def current_ability
        @_current_ability ||= Ability.new(current_user)
      end

      def authenticate_user_from_token
        token = params[:token].presence
        token && User.find_by(token: token)
      end

      def authorize!(action, model)
        error!('403 Forbidden', 403) unless current_ability.can?(action, model)
      end

      def authenticated?
        !!current_user
      end
    end

    rescue_from ActiveRecord::RecordNotFound do
      rack_response( {status: 404, message: 'not found'}.to_json, 404)
    end

    rescue_from Grape::Exceptions::ValidationErrors do |e|
      rack_response({ status: e.status, message: e.message }.to_json, e.status)
    end


    mount Users
    mount Timelines
    mount Milestones
    mount Labels
    mount SubItems
    mount Roadmaps
    mount UserRoadmaps
    mount Activities
    mount Subscriptions

    add_swagger_documentation(
        base_path: "#{ActionController::Base.asset_host}/api",
        api_version: self.version,
        hide_documentation_path: true,
    )

    route :any, '*path' do
      error!({ status: 404, message: 'not found' }, 404)
    end
  end
end